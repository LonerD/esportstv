//
//  CustomCellChmpsh.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 25.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class CustomCellChmpsh: UITableViewCell {

    @IBOutlet weak var imageChmpsh: UIImageView!
    @IBOutlet weak var titleChmpsh: UILabel!
    @IBOutlet weak var prizeFundChmpsh: UILabel!
    
    @IBOutlet weak var goToStreamsButtons: UIButton!
    
    @IBOutlet weak var dateStart: UILabel!
    @IBOutlet weak var dateEnd: UILabel!
    @IBOutlet weak var descriptionAboutEvent: UITextView!

    @IBOutlet weak var isLive: UIImageView!

    
}
