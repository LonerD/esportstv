//
//  ListCategoriesPad.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 09.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

protocol categoriesDelegate {
    func didParse(finish: Bool)
}

class ListCategoriesPad: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate {
    
    var delegate: categoriesDelegate!
    
    let categoriesXMLLink = host + "/Categories"
    
    var parser: XMLParser!
    let downloadTheImage = ImageDownloader()
    var categotiesMembers = [categoriesData]()
    
    struct categoriesData {
        var headingsOfTheGame: String
        var XMLLinkOfTheGame: String
        var title: String
        var subscribe: Following = .unfollow
        
        mutating func subscribeManager(_ type: String) {
            switch type {
            case "0": subscribe = .unfollow
            default: subscribe = .follow
            }
        }
        
    }
    
    //startPageProgress
    var startViewController = UIViewController()
    fileprivate var progressNumber = 0
    fileprivate var statusOfTheEnd = 0
    
    //Outlets
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    
    var size: CGFloat!
    
    //VC LifeCicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.delegate = self
        callStarVC()
        if token != nil {
            parseCatigories()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notifCentr.addObserver(self, selector: #selector(ListCategories.parseCatigories), name: Notification.Name(rawValue: "reloadCategories"), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        settingView()
    }
    
    func setCollectionViewLayoutDirection(_ direction: Direction) {
         let layout = self.collectionViewCategories.collectionViewLayout as! UICollectionViewFlowLayout
        switch direction {
        case .hor :
            size = (view.bounds.size.width - 20 * 8) / 4
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        case .ver :
            
            size = (view.bounds.size.width - 20 * 6) / 3
        
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
            
        }
        
        kMainQueue.async {
            self.collectionViewCategories.collectionViewLayout.invalidateLayout()
        }
        
    }
    
    
    
    
    //CollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categotiesMembers.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewCategories.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ListCategoriesCVCellPad
        
        let urlString = categotiesMembers[indexPath.row].headingsOfTheGame
        
        downloadImage(urlString) { (image) in
            cell.imageOfTheGame.image = image
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: size, height: size)

    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionViewCategories.cellForItem(at: indexPath)
        UIView.animate(withDuration: 0.2, animations: {
            cell?.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        })
        return true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionViewCategories.cellForItem(at: indexPath)
        UIView.animate(withDuration: 0.2, animations: {
            cell?.transform = CGAffineTransform.identity
        })
    }
    
    
    
    
    func settingView() {
        view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundTV")!)
        self.navigationController?.navigationBar.isHidden = true
        
        let oritntation = view.checkOrientation()
        setCollectionViewLayoutDirection(oritntation)
       
        
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController == self {
        settingView()
        }
    }
    
    func callStarVC() {
        startViewController = self.storyboard?.instantiateViewController(withIdentifier: "startScreen") as! StartScreen
        let currentVindow = UIApplication.shared.keyWindow
        currentVindow?.addSubview(startViewController.view)
    }
    
    func deleteStartVC() {
        self.startViewController.view.removeFromSuperview()
    }

    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toChmpsh" {
            if let cell = sender as? UICollectionViewCell {
                if let indexPath = collectionViewCategories.indexPath(for: cell) {
                let destVC = segue.destination as! ListChampshPad
                destVC.linkXMLToParse = categotiesMembers[indexPath.row].XMLLinkOfTheGame
                destVC.subtitle = categotiesMembers[indexPath.row].title
                destVC.index = indexPath.row
                destVC.parentVC = self
                switch categotiesMembers[indexPath.row].subscribe {
                    case .follow: destVC.isFollow = true
                    case .unfollow: destVC.isFollow = false
                }
                }
            }
        } else if segue.identifier == "slider" {
            let destVC = segue.destination as! SliderPad
            destVC.categories = self
        }
    }
    
    
    


}

extension ListCategoriesPad: XMLParserDelegate {
    
    func XMLParserError(_ error: String) {
        notifCentr.post(name: Notification.Name(rawValue: "parseError"), object: nil)
    }
    
    
    func parseCatigories() {
        parser = XMLParser(url: categoriesXMLLink)
        parser.delegate = self
        parser.parse {
            
            for i in 0..<self.parser.objects.count {
                self.categotiesMembers.append(categoriesData(
                    headingsOfTheGame: self.parser.objects[i]["imgPad"]!.toUTF8,
                    XMLLinkOfTheGame: self.parser.objects[i]["linkXML"]!.toUTF8,
                    title: self.parser.objects[i]["title"]!.toUTF8, subscribe: .unfollow))
                self.downloadImage(self.categotiesMembers[i].headingsOfTheGame, image: nil)
                self.categotiesMembers[i].subscribeManager(self.parser!.objects[i]["subscribe"]!.toUTF8)
            }
            self.delegate.didParse(finish: true)
            self.statusOfTheEnd = 100 / self.categotiesMembers.count
            
        }
    }
    
    func downloadImage(_ imageLink: String, image: ((UIImage) -> ())?) {
        var progress = Float()
        downloadTheImage.downloadImages(imageLink, ifPhotoAvailable: { (downloadedImage) in
            image?(downloadedImage)
        }) { [unowned self] (downloadedImage) in
            guard self.progressNumber < self.categotiesMembers.count else {
                image?(downloadedImage!)
                return
            }
            // Start display ProgressBar
            self.progressNumber += 1
            progress = Float(self.statusOfTheEnd * self.progressNumber) / 100
            let reqiredUserInf = ["progress" : progress]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "passDataInView"), object: nil, userInfo: reqiredUserInf)
            if progress >= 0.99 {
                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ListCategories.deleteStartVC), userInfo: nil, repeats: false)
                self.collectionViewCategories.reloadData()
            }
            image?(downloadedImage!)
        }
    }
    
}




