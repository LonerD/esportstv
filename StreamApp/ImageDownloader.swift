//
//  ImageDownloader.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 22.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

var imgDict = [String: UIImage]()

struct ImageDownloader {
    
    func downloadImages(_ photoLink: String, ifPhotoAvailable: ((UIImage)->())?, ifPhotoUnavailable: ((UIImage?)->())?){
        if let img = imgDict[photoLink] {
            ifPhotoAvailable!(img)
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            if let imageURL = URL(string: photoLink) {
                kBgQ.async(execute: { () -> Void in
                    if let theData = try? Data(contentsOf: imageURL) {
                        if let image = UIImage(data: theData) {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            kMainQueue.async(execute: {
                                imgDict[photoLink] = image
                                ifPhotoUnavailable!(image)
                            })
                        }
                    } else {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        print("I cant download this image :(")
                    }
                })
            }
        }
    }
    
}










