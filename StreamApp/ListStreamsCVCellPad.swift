//
//  ListStreamsCVCellPad.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 22.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class ListStreamsCVCellPad: UICollectionViewCell {
    
    @IBOutlet weak var languageOfTheStream: UILabel!
    
    @IBOutlet weak var imageBackgroundView: UIView!
    
    @IBOutlet weak var imageStreams: UIImageView!
    
    @IBOutlet weak var nameOfTheStream: UILabel!
    
}
