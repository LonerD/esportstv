//
//  CollectionViewFlowLayout.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 15.07.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class CollectionViewFlowLayout: UICollectionViewFlowLayout {

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = super.layoutAttributesForElements(in: rect)! as [UICollectionViewLayoutAttributes]
        
        let headersNeedingLayout = NSMutableIndexSet()
        
        for attributes in layoutAttributes {
            if attributes.representedElementCategory == .cell {
                headersNeedingLayout.add((attributes.indexPath as NSIndexPath).section)
            }
        }
        for attributes in layoutAttributes {
            if let elementKind = attributes.representedElementKind {
                if elementKind == UICollectionElementKindSectionHeader {
                    headersNeedingLayout.remove((attributes.indexPath as NSIndexPath).section)
                }
            }
        }
        
        
        for index in headersNeedingLayout {
            let indexPath = IndexPath(item: 0, section: index)
            let attributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath)
            layoutAttributes.append(attributes!)
        }
        

        
        for attributes in layoutAttributes {
            if let elementKind = attributes.representedElementKind {
                if elementKind == UICollectionElementKindSectionHeader {
                    let section = (attributes.indexPath as NSIndexPath).section
                    
                    let attributesForFirstItemInSection = layoutAttributesForItem(at: IndexPath(item: 0, section: section))
                    
                    let attributesForLastItemInSection = layoutAttributesForItem(at: IndexPath(item: collectionView!.numberOfItems(inSection: section), section: section))
                    
                    
                    var frame = attributes.frame
                    let offset = collectionView?.contentOffset.y
                    
                    let minY = attributesForFirstItemInSection!.frame.minY - frame.height
                    let maxY = attributesForLastItemInSection!.frame.minY - frame.height
                    
                    let y = min(max(offset!, minY), maxY)
                    frame.origin.y = y
                    attributes.frame = frame
                    attributes.zIndex = 99
                    
                    
                    
                }
            }
        }
        
        return layoutAttributes
        
        
    }
}
