//
//  SliderPad.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 12.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import SafariServices



class SliderPad: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, categoriesDelegate {

    @IBOutlet weak var collectionViewSliderPad: UICollectionView!
    var categories: ListCategoriesPad!
    
    let sliderXMLParse = host + "/Slider"
    var parser: XMLParser!
    let downloadImage = ImageDownloader()
    
    struct sliderData {
        var pageImage: String
        var pageStreamLink: String
        var pageAdvLink: String
    }
    
    var sliderMembers = [sliderData]()
    
    var currentPage: CGFloat!
    
    
    //Slide adv
    var timer: Timer!
    
    var sliderCount: Int = 0 {
        didSet {
            if sliderMembers.count > 1 {
                self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(SliderPad.slide), userInfo: nil, repeats: false)
                
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categories.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if sliderMembers.count != 0 {
            currentPage = self.collectionViewSliderPad.contentOffset.x / self.collectionViewSliderPad.frame.width
            self.collectionViewSliderPad.collectionViewLayout.invalidateLayout()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if currentPage != nil && Int(currentPage) != 0 {
            print(Int(currentPage))
        self.collectionViewSliderPad.scrollToItem(at: IndexPath(item: Int(currentPage), section: 0), at: .left, animated: false)
        }
    }

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        sliderCount = 0
        return sliderMembers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewSliderPad.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SliderPadCVCell
        
        let imageLint = sliderMembers[indexPath.row].pageImage
        downloadImage.downloadImages(imageLint, ifPhotoAvailable: { (image) in
            cell.sliderImage.image = image
        }) { (image) in
            cell.alpha = 0
            cell.sliderImage.image = image
            UIView.animate(withDuration: 0.3) {
                cell.alpha = 1
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 0.3) { 
            cell.alpha = 1
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width, height: view.frame.size.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard sliderMembers[indexPath.row].pageAdvLink == "nil" else {
            let advertisementURL = sliderMembers[indexPath.row].pageAdvLink
            if #available(iOS 9.0, *) {
                let safaryVC = SFSafariViewController(url: URL(string: advertisementURL)!)
                safaryVC.delegate = self
                 UIApplication.shared.keyWindow?.rootViewController?.present(safaryVC, animated: true, completion: nil)
            } else {
            UIApplication.shared.openURL(URL(string: advertisementURL)!)
            }
            return
        }
        performSegue(withIdentifier: "toStreams", sender: indexPath)
        
    }
    
    /*
     *
     * This function slide advertisemant menu (collection view) by variable: var sliderCount in the head of the class
     */
    func slideAdv(_ number: Int) {
        guard sliderMembers.count != 0 else {
            return
        }
        
        let indexToScroll = IndexPath(row: sliderCount, section: 0)
        if sliderCount < (sliderMembers.count - 1) {
            collectionViewSliderPad.scrollToItem(at: indexToScroll, at: .left, animated: true)
            sliderCount += 1
        } else {
            self.sliderCount = 0
             self.collectionViewSliderPad.scrollToItem(at: indexToScroll, at: .left, animated: true)
        }
    }
    
    func slide() {
        slideAdv(sliderCount)
        
    }
    
    //DelegateMethods 
    
    func didParse(finish: Bool) {
        if finish {
            parse(linkToParse: sliderXMLParse)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStreams" {
            if let indexPath = sender as? IndexPath {
                let destVC = segue.destination as! Player
                destVC.linksArray = (sliderMembers[indexPath.row].pageStreamLink).components(separatedBy: ", ")
            }
        }
    }


}

extension SliderPad: XMLParserDelegate {
    
    func XMLParserError(_ error: String) {
       
    }
    
    func parse(linkToParse: String) {
        self.parser = XMLParser(url: linkToParse)
        
        self.parser!.parse {
            self.sliderMembers = []
            for i in 0..<self.parser.objects.count {
                self.sliderMembers.append(sliderData(
                    pageImage: self.parser.objects[i]["imgPad"]!.toUTF8,
                    pageStreamLink: self.parser.objects[i]["linkStream"]!,
                    pageAdvLink: self.parser.objects[i]["linkAdv"]!.toUTF8))
                
            }
            
            self.collectionViewSliderPad.reloadData()
        }
    }
}


extension SliderPad: SFSafariViewControllerDelegate {
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
