//
//  DetailInfoTF.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 13.07.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class DetailInfoTF: UITextView, NSLayoutManagerDelegate {
    
    var firstShow = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutManager.delegate = self
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        if firstShow {
            self.setContentOffset(CGPoint.zero, animated: false)
            firstShow = false
        }
    }
    
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        
        return 4
    }
    
    
    
    
}
