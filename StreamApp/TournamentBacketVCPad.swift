//
//  TournamentBacketVCPad.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 26.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class TournamentBacketVCPad: UIViewController {
    
    var tapGesture: UITapGestureRecognizer!
        
        var parser: XMLParser!
    
        //Activity Indicatior
        var activIndicator = FPActivityLoader()
    
        //Outlets
        
        @IBOutlet weak var segmentControl: UIView!
        
        struct commands {
            var commandOne: String
            var commandOneImg: String
            var commandOneScrore: String
            var commandTwo: String
            var commandTwoImg: String
            var commandTwoScrore: String
            var date: String
            var time: String
            var bestOf: String
        }
        
        
        
        let downloadImage = ImageDownloader()
        
        var groups = [String : [commands]]()
        var playOff = [String : [commands]]()
        
        var playOffShow = Bool()
        var sectionTint: [UIColor]!
        
        @IBOutlet weak var collectionView: UICollectionView!
        
        var linkXMLToParse: String!
        
        var navigationTitle = String()
        
        
        //LifeCycle
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.preferredContentSize = CGSize(width: UIScreen.main.applicationFrame.width / 1.2, height: UIScreen.main.applicationFrame.height / 1.2)
            
            self.segmentControl.layer.opacity = 0
            
            self.parseTornaments(linkXMLToParse)
            
        }
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            collectionView.collectionViewLayout.invalidateLayout()
            animate(playOffShow, false)
        }
    
    
        
        override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
            collectionView.collectionViewLayout.invalidateLayout()
            self.preferredContentSize = CGSize(width: UIScreen.main.applicationFrame.height / 1.2, height: UIScreen.main.applicationFrame.width / 1.2)
        }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createActivityView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         tapGesture = UITapGestureRecognizer(target: self, action: #selector(TournamentBacketVCPad.tapBehindDetected(sender:)))
        tapGesture.delegate = self
        self.view.window?.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.window?.removeGestureRecognizer(tapGesture)
    }
    

    
    func tapBehindDetected(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            let location = sender.location(in: nil)
            
            if !(self.view.point(inside: (self.view.convert(location, from: self.view.window)), with: nil)) {
                    self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //Buttons
    
    
    @IBAction func dismissView(_ sender: AnyObject) {
        dismiss(animated: true , completion: nil)
    }

    
        //Change Buttons
        
        @IBOutlet weak var changerConstr: NSLayoutConstraint!
        
        @IBOutlet weak var groupMatches: UIButton!
        @IBAction func groupMatches(_ sender: UIButton) {
            animate(false)
        }
        
        @IBOutlet weak var playoffMatches: UIButton!
        @IBAction func playoffMatches(_ sender: UIButton) {
            animate(true)
        }
        
        func animate(_ playOffShowing: Bool, _ reload: Bool = true) {
            UIView.animate(withDuration: 0.3, animations: {
                switch playOffShowing {
                case false:
                    self.groupMatches.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    self.playoffMatches.transform = CGAffineTransform.identity
                    self.playOffShow = false
                case true:
                    self.groupMatches.transform = CGAffineTransform.identity
                    self.playoffMatches.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    self.playOffShow = true
                }
                self.changerConstr.constant = (UIScreen.main.bounds.size.width / 1.2 - 180) / 2 * CGFloat(playOffShowing.hashValue)
                
                
            })
            if reload {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
                sectionTint = []
                self.collectionView.setContentOffset(CGPoint.zero, animated: true)
                
                self.collectionView.reloadData()
                
                
            }
            
        }
    
    func createActivityView() {

        
        activIndicator.circleTime = 2
        activIndicator.frame.size = CGSize(width: 100, height: 100)
        activIndicator.center = CGPoint(x: UIScreen.main.applicationFrame.width / 1.2 / 2, y: UIScreen.main.applicationFrame.height / 1.2 / 2)
        activIndicator.strokeColor = UIColor(red: 255/255, green: 167/255, blue: 52/255, alpha: 1)
        activIndicator.animating = true
        view.addSubview(activIndicator)
        
    }
    
    func createErrorView(text toShow: String) {
        let errorView = ErrorView(frame: self.view.frame, textToShow: toShow)
        errorView.center = view.center
        self.view.insertSubview(errorView, at: 1)

        
        
    }
    
    }

    extension TournamentBacketVCPad: UICollectionViewDelegate, UICollectionViewDataSource {
        
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            let sectionsNumber = playOffShow ? playOff.keys.count : groups.keys.count
            
            return sectionsNumber
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            let keys = playOffShow ? [String] (playOff.keys).sorted() : [String] (groups.keys).sorted()
            
            let countForEachKey = playOffShow ? playOff[keys[section]]?.count : groups[keys[section]]?.count
            
            
            let rowsNumb = countForEachKey ?? 0

            return rowsNumb
            
        }
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
            return CGSize(width: ((self.collectionView?.frame)!.width - 200) / 2, height: 143)
        }
        
        
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "section", for: indexPath) as! SectionReusableView
            
            let keys = playOffShow ? [String] (playOff.keys).sorted() : [String] (groups.keys).sorted()
            
            reusableView.title.text = keys[(indexPath as NSIndexPath).section]
            
            return reusableView
        }
        
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TournamentCustomCell
            
            let keys = playOffShow ? [String] (playOff.keys).sorted() : [String] (groups.keys).sorted()
            
            let dataInSection = playOffShow ? playOff[keys[indexPath.section]] : groups[keys[(indexPath as NSIndexPath).section]]
            
            let cestDate = dataInSection![indexPath.row].date.clearSpace + " " + dataInSection![indexPath.row].time.clearSpace
            
            cell.timeTournament.text = cestDate.cestToCurrentTime.0 ?? dataInSection![indexPath.row].time.clearSpace + " CEST"
            cell.dateTournament.text = cestDate.cestToCurrentTime.1 ?? dataInSection![indexPath.row].date.clearSpace
            
            cell.bestOf.text = "Best of " + dataInSection![indexPath.row].bestOf
            
            let urlStringCommOne = dataInSection![indexPath.row].commandOneImg.clearSpace.toUTF8
            
            cell.firstCommandImg.image = UIImage(named: "loading")
            cell.secondCommandImg.image = UIImage(named: "loading")
            
            downloadImage.downloadImages(urlStringCommOne, ifPhotoAvailable: { (image) in
                cell.firstCommandImg.image = image
            }) { (image) in
                cell.firstCommandImg.alpha = 0
                cell.firstCommandImg.image = image
                UIView.animate(withDuration: 0.3, animations: {
                    cell.firstCommandImg.alpha = 1
                })
            }
            
            
            
            cell.firstCommandScore.text = dataInSection![indexPath.row].commandOneScrore
            cell.firstCommandTitle.text = dataInSection![indexPath.row].commandOne
            
            let imgUrlCommTwo = dataInSection![indexPath.row].commandTwoImg.clearSpace.toUTF8
            
            downloadImage.downloadImages(imgUrlCommTwo, ifPhotoAvailable: { (image) in
                cell.secondCommandImg.image = image
            }) { (image) in
                cell.secondCommandImg.alpha = 0
                cell.secondCommandImg.image = image
                UIView.animate(withDuration: 0.3, animations: {
                    cell.secondCommandImg.alpha = 1
                })
            }
            
            cell.secondCommandScore.text = dataInSection![indexPath.row].commandTwoScrore
            cell.secondCommandTitle.text = dataInSection![indexPath.row].commandTwo
            
            
            if sectionTint.indices.contains(indexPath.section) {
                cell.backgroundColor = sectionTint[indexPath.section]
                cell.timeInfoSubview.backgroundColor = sectionTint[indexPath.section]
            } else {
                while !sectionTint.indices.contains(indexPath.section) {
                    let colorSection = UIColor().randomColor(sectionTint).withAlphaComponent(0.3)
                    
                    sectionTint.append(colorSection)
                }
                
                
                cell.backgroundColor = sectionTint[indexPath.section]
                cell.timeInfoSubview.backgroundColor = sectionTint[indexPath.section]
                
                
            }
            
            
            
            
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            cell.alpha = 0
            UIView.animate(withDuration: 0.3) { 
                cell.alpha = 1
            }
        }
        
    }
    
    extension TournamentBacketVCPad: XMLParserDelegate {
        
        func XMLParserError(_ error: String) {
            kMainQueue.async {
                self.createErrorView(text: "There are problems with the connection to the server")
                self.activIndicator.animating = false
            }
        }
        
        func parseTornaments(_ linkToParse: String) {
            parser = XMLParser(url: linkToParse)
            parser.delegate = self
            
            self.parser.parse {
                
                
                
                for i in 0..<self.parser.objects.count {
                    
                    if self.parser.objects[i]["Category"]?.clearSpace == "GROUPS" {
                        if self.groups[self.parser.objects[i]["section"]!] == nil {
                            
                            self.groups[self.parser.objects[i]["section"]!] = [commands(
                                commandOne: self.parser.objects[i]["commandOne"]!,
                                commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                                commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                                commandTwo: self.parser.objects[i]["commandTwo"]!,
                                commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                                commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                                date: self.parser.objects[i]["date"]!,
                                time: self.parser.objects[i]["time"]!,
                                bestOf: self.parser.objects[i]["bestOf"]!)]
                        } else {
                            var sectionCommands = self.groups[self.parser.objects[i]["section"]!]
                            sectionCommands?.append(commands(
                                commandOne: self.parser.objects[i]["commandOne"]!,
                                commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                                commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                                commandTwo: self.parser.objects[i]["commandTwo"]!,
                                commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                                commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                                date: self.parser.objects[i]["date"]!,
                                time: self.parser.objects[i]["time"]!,
                                bestOf: self.parser.objects[i]["bestOf"]!))
                            
                            self.groups[self.parser.objects[i]["section"]!] = sectionCommands
                            
                        }
                        
                    } else if self.parser.objects[i]["Category"]?.clearSpace == "PLAYOFF" {
                        
                        if self.playOff[self.parser.objects[i]["section"]!] == nil {
                            
                            self.playOff[self.parser.objects[i]["section"]!] = [commands(
                                commandOne: self.parser.objects[i]["commandOne"]!,
                                commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                                commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                                commandTwo: self.parser.objects[i]["commandTwo"]!,
                                commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                                commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                                date: self.parser.objects[i]["date"]!,
                                time: self.parser.objects[i]["time"]!,
                                bestOf: self.parser.objects[i]["bestOf"]!)]
                            
                        } else {
                            var sectionCommands = self.playOff[self.parser.objects[i]["section"]!]
                            sectionCommands?.append(commands(
                                commandOne: self.parser.objects[i]["commandOne"]!,
                                commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                                commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                                commandTwo: self.parser.objects[i]["commandTwo"]!,
                                commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                                commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                                date: self.parser.objects[i]["date"]!,
                                time: self.parser.objects[i]["time"]!,
                                bestOf: self.parser.objects[i]["bestOf"]!))
                            
                            self.playOff[self.parser.objects[i]["section"]!] = sectionCommands
                        }
                    }
                }
                
                self.animateEmptyList()
                
            }
        }
        
        func animateEmptyList() {
            activIndicator.animating = false
            if self.groups.count == 0 && self.playOff.count == 0  {
                createErrorView(text: "Tournament bracket isn't available yet")
            } else {
                checkingContentAvailability()
            }
            
        }
        
        
        func checkingContentAvailability() {
            segmentControl.layer.opacity = 1
            switch self.groups.count {
            case 0: playOffShow = true
            groupMatches.isEnabled = false
            groupMatches.alpha = 0.5
            default:
                playOffShow = false
                if playOff.count == 0 {
                    playoffMatches.isEnabled = false
                    playoffMatches.alpha = 0.5
                }
            }
            
            animate(self.playOffShow)
        }
        
        
       
        
    }

extension TournamentBacketVCPad: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
}



