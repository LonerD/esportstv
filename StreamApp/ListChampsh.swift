//
//  listChampsh.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 22.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ListChampsh: UITableViewController, XMLParserDelegate {
    
    //AdMob Service
    var advContentView = [UIView]()
    weak var adView: AdvManager!
    
    var parser: XMLParser!
    
    var alertNotifyView: NotificationViewController?
    
    //Notification Block
    weak var parentVC: ListCategories?
    
    var index: Int!
    var isFollow = Bool() {
        didSet {
            guard parentVC != nil && index != nil else {
                return
            }
            parentVC!.categotiesMembers[index].subscribeManager(isFollow ? "1" : "0")
            setupNavBar()
        }
    }
    
    ///Data From Server
    
    let downloadImage = ImageDownloader()
    
    struct eventsData {
        var events: String
        var imageLink: String
        var prizeFund: String
        var eventStreamsXML: String
        
        var subtitle: String
        
        var startDate: String
        var endDate: String
        var description: String
        var linkXMLTournament: String
        
        var subscribe: Following = .unfollow
        var isLive: Bool
        
        mutating func subscribeManager(_ type: Int) {
            switch type {
            case 0: subscribe = .unfollow
            default: subscribe = .follow
            }
        }
        
        mutating func eventStreamsIsLiveManager(_ type: Int) {
            switch type {
            case 0: isLive = false
            default: isLive = true
            }
        }
        
    }
    
    var eventMembers = [eventsData?]()
    
    var indexPathSelect: IndexPath!
    
    ////parse String from Server
    var XMLStringChmpsh = String()
    var navigationTitle = String()
    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressView.setProgress(0.3, animated: true)
        
        title = navigationTitle + " EVENTS"
        parse(XMLStringChmpsh)
        setupNavBar()


        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        if indexPathSelect != nil {
            let indexPath = IndexPath(row: indexPathSelect.row, section: 0)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    func XMLParserError(_ error: String) {
        kMainQueue.async {
            self.showUnavailability("There are problems with the connection to the server")
        }
    }
    
    func setupNavBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.backgroundColor = UIColor.init(patternImage: UIImage(named: "backgroundTV")!)
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: isFollow ? UIImage(named: "bellIsFollowed")! : UIImage(named: "bellIsUnfollowed")!, style: .plain, target: self, action: #selector(ListChampsh.notificationSubscribe))
    }
    
    func notificationSubscribe() {
        alertNotifyView = NotificationViewController(nibName: "NotificationView", bundle: nil)
        
        alertNotifyView?.isFollowed = isFollow
        
        if let targetView = navigationItem.rightBarButtonItem?.value(forKey: "view") {
            alertNotifyView?.centerOfTheStart = (targetView as AnyObject).center
        }
        
        alertNotifyView?.referanceVC = self
        alertNotifyView?.linkToSubscribe = XMLStringChmpsh
        alertNotifyView?.textToAlert = "Do you want to " + (isFollow ? "unfollow " : "follow ") + navigationTitle + "?"
        
        
        alertNotifyView!.view.frame = self.view.frame
        alertNotifyView!.showInView(self.view)
        
        
    }
    
    func parse(_ linkToParse: String) {
        parser = XMLParser(url: linkToParse)
        parser.delegate = self
        
        parser.parse {
            if self.parser.objects.count != 0 {
                for i in 0..<self.parser.objects.count {
                    self.eventMembers.append(eventsData(
                        events: self.parser.objects[i]["title"]!.clearSpace,
                        imageLink: self.parser.objects[i]["img"]!.toUTF8,
                        prizeFund: self.parser.objects[i]["PrizeFund"]!,
                        eventStreamsXML: self.parser.objects[i]["linkXMLToStreams"]!.toUTF8,
                        subtitle: self.parser.objects[i]["subtitle"]!.toUTF8,
                        startDate: self.parser.objects[i]["StartDate"]!,
                        endDate: self.parser.objects[i]["EndDate"]!,
                        description: self.parser.objects[i]["Description"]!,
                        linkXMLTournament: self.parser.objects[i]["linkXMLTournamentBracket"]!.toUTF8,
                        subscribe: .unfollow,
                        isLive: false
                        ))
                    
                self.eventMembers[i]?.subscribeManager(Int(self.parser.objects[i]["subscribe"]!.toUTF8)!)
                    
                self.eventMembers[i]?.eventStreamsIsLiveManager(Int(self.parser.objects[i]["live"]!.toUTF8)!)
                
                    
                }
                
                //Creating adverticement cells
                    self.createAdvCells()
                
                    self.tableView.reloadData()
                
                    UIView.animate(withDuration: 0.3, animations: {
                        self.progressView.setProgress(1, animated: true)
                    })
                    
                    
                    UIView.animate(withDuration: 0.3, delay: 0.3, options: [], animations: {
                        self.tableView.contentInset = UIEdgeInsetsMake(61, 0, 0, 0)
                        self.progressView.alpha = 0
                        }, completion: nil)
                
            } else {
                self.showUnavailability("There are no events available yet")
            }
        }
    }
    
    
    func showUnavailability(_ text: String) {
        let errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 64), textToShow: text)
        self.view.addSubview(errorView)
        
        self.progressView.setProgress(0.8, animated: true)
        
        self.tableView.isScrollEnabled = false
        UIView.animate(withDuration: 1, animations: {
            self.progressView.setProgress(0, animated: true)
        }) 
    }


    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStreams" {
            let destVC = segue.destination as! ListStreams
            destVC.linkXMLStreams = eventMembers[indexPathSelect.row]!.eventStreamsXML
            destVC.navigationTitle = eventMembers[indexPathSelect.row]!.subtitle
            
            destVC.index = indexPathSelect.row
            destVC.parentVC = self
            
            switch eventMembers[indexPathSelect.row]!.subscribe {
            case .follow: destVC.isFollow = true
            case .unfollow: destVC.isFollow = false
            }
        } else if segue.identifier == "TournamentBracket" {
            let destVC = segue.destination as! TournamentBracketVC
            destVC.linkXMLToParse = eventMembers[indexPathSelect.row]!.linkXMLTournament
            destVC.navigationTitle = eventMembers[indexPathSelect.row]!.subtitle
            
        }
    }
    
    
    
    
}


//TableView Data Source

extension ListChampsh {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventMembers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellChmpsh", for: indexPath) as! CustomCellChmpsh
        ////Set cells data
        
        cell.selectionStyle = .default
        
        for subview in cell.subviews {
            if subview.tag == 1 {
                subview.removeFromSuperview()
            }
        }
        
        guard eventMembers[indexPath.row] != nil else {
            cell.selectionStyle = .none
            
            
            var count = -1
            for (index, element) in eventMembers.enumerated() {
                if element == nil {
                    count += 1
                    if index == (indexPath as NSIndexPath).row {
                        if !advContentView.indices.contains(count) {
                            adView = AdvManager(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100), viewController: self)
                            adView.translatesAutoresizingMaskIntoConstraints = false
                            adView.center.x = cell.center.x
                            adView.tag = 1
                            advContentView.append(adView)
                        }
                        cell.addSubview(advContentView[count])
                        
                        let views = ["advView" : advContentView[count], "cell" : cell.contentView]
                        
                        cell.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[advView(==cell)]|", options: [], metrics: nil, views: views))
                        cell.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[advView]|", options: [], metrics: nil, views: views))
                    }
                }
            }
            return cell
        }
        
        cell.titleChmpsh.text = eventMembers[indexPath.row]!.events
        cell.imageChmpsh.image = UIImage(named: "loading")
        
        //Download Image
        let urlString = eventMembers[indexPath.row]!.imageLink
        downloadImage.downloadImages(urlString, ifPhotoAvailable: { (image) in
            cell.imageChmpsh.image = image
        }) { (image) in
            cell.imageChmpsh.alpha = 0
            cell.imageChmpsh.image = image
            UIView.animate(withDuration: 0.3, animations: {
                cell.imageChmpsh.alpha = 1
            })
        }
        
        
        cell.prizeFundChmpsh.text = "$ " + eventMembers[indexPath.row]!.prizeFund
        
        cell.dateStart.text = eventMembers[indexPath.row]!.startDate
        cell.dateEnd.text = eventMembers[indexPath.row]!.endDate
        cell.descriptionAboutEvent.text = eventMembers[indexPath.row]!.description
        cell.descriptionAboutEvent.setContentOffset(CGPoint.zero, animated: false)
        
        switch eventMembers[indexPath.row]!.isLive {
            case true: cell.isLive.isHidden = false
            case false: cell.isLive.isHidden = true
        }
        
        
        
        return cell
    }
    
    
    func createAdView() {
        
    }
    
    //Implementring animation for detail cell
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard eventMembers[indexPath.row] != nil else {
            return
        }
        
        
        self.progressView.isHidden = true
        if indexPathSelect != nil && indexPathSelect == indexPath {
            indexPathSelect = nil
            
            tableView.beginUpdates()
            tableView.endUpdates()
            tableView.deselectRow(at: indexPath, animated: true)
            
        } else {
            indexPathSelect = indexPath
            tableView.beginUpdates()
            tableView.endUpdates()
            
            let delayTime = DispatchTime.now() + 0.17
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPathSelect != nil && indexPath ==  indexPathSelect {
                return 402
        } else {
            return 100
        }
    }
    
}

//Implementing Advertisenemts
extension ListChampsh {
    func createAdvCells() {
        let visibleCells: Int = Int(self.view.frame.size.height) / 100
        
        let random = self.eventMembers.count < visibleCells ? Int(arc4random_uniform(UInt32(self.eventMembers.count))) : 3
        
        self.eventMembers.insert(nil, at: random)
        
        for index in random...self.eventMembers.count {
            if index % visibleCells == 0 && index != 0 && eventMembers.indices.contains(index + random) {
                self.eventMembers.insert(nil, at: index + random)
            }
        }
    }
    
}





