
//
//  AppDelegate.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 21.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    enum storyboardName: String {
        case iPhone = "iPhone"
        case iPad = "iPad"
    }
    
    var window: UIWindow?
    //    var timer: NSTimer?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        //        Override point for customization after application launch.
        let pageController = UIPageControl.appearance()
        pageController.hidesForSinglePage = true
        pageController.defersCurrentPageDisplay = false
        
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().tintColor = UIColor.white
        
        UITableView.appearance().tableFooterView = UIView()
        
        if let barFont = UIFont(name: "HeliosCond", size: 20) {
            UINavigationBar.appearance().titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: barFont]
        }
        
        let image = UIImage(named: "header")
        UINavigationBar.appearance().setBackgroundImage(image, for: UIBarMetrics.default)
        
        if #available(iOS 10, *) {
            let notificationCenter = UNUserNotificationCenter.current()
            notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { (finished, err) in
                if err == nil {
                    application.registerForRemoteNotifications()
                }
            
        }
    }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        self.window?.rootViewController = deviceDetector().0?.instantiateInitialViewController()
        return true
    }
    
    

    
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        imgDict.removeAll()
    }
    
    //Remote Notification block
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       
        let deviceTokenString = deviceToken.hexString()

        if userDef.object(forKey: "deviceTokenString") == nil {
            request(deviceTokenString, device: deviceDetector().1?.rawValue)
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    
    print("Couldn’t register: \(error)")
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    }
    
    
    
    func request(_ deviceToken: String, device: String?) {
        let manager = RequestManager(urlForRequest: host + "/add-device?", parametrs: ["token": deviceToken, "device" : device!], reqType: .Get)
        
        manager.requestTask { (response, error) in
            guard response != nil else {
                print(error)
                                  
                return
            }
            print(response)
            
            userDef.set(deviceToken, forKey: "deviceTokenString")
            token = userDef.string(forKey: "deviceTokenString")
            notifCentr.post(name: Notification.Name(rawValue: "reloadCategories"), object: nil)
        }
    }
    
    func deviceDetector() -> (UIStoryboard?, storyboardName?) {
        let device = UIDevice.current
        
        var currentStoryboard: UIStoryboard!
        var deviceType: storyboardName!
        
        switch device.userInterfaceIdiom {
        case .phone:
            currentStoryboard = UIStoryboard(name: storyboardName.iPhone.rawValue, bundle: nil)
            deviceType = .iPhone
            
        case .pad:
            currentStoryboard = UIStoryboard(name: storyboardName.iPad.rawValue, bundle: nil)
            deviceType = .iPad
        default: return (nil, nil)
        }
        
        return (currentStoryboard, deviceType)
    }
    
    
}

