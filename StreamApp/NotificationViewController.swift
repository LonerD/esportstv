//
//  NotificationView.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 29.08.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var alertMassage: UILabel!
    
    var parentVC: UIViewController!
    var referanceVC: UIViewController!
    var index = Int()
    
    var centerOfTheStart: CGPoint!
    
    var textToAlert: String!
    var isFollowed = false
    var linkToSubscribe: String!
    
    var coordinatorModel: NotifyCoordinator!
    var appearanceModel: NotificationAppearance! {
        didSet {
            subscribe.setTitle(appearanceModel.title, for: UIControlState())
            subscribe.backgroundColor = appearanceModel.color
        }
    }
    
    
    var window: UIWindow?
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coordinatorModel = NotifyCoordinator(coordinatorDidUpdateStateModelBlock: { (model) in
            self.appearanceModel = model
        })
        coordinatorModel.setModel(isFollowed)
        
        self.alertView.layer.position = centerOfTheStart
        alertMassage.text = textToAlert
        alertView.transform = CGAffineTransform(scaleX: 0, y: 0)
        alertView.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundTV")!)
        self.alertView.clipsToBounds = true
        self.alertView.layer.cornerRadius = 10
        
        
    }
    
    
    func showInView(_ parentView: UIView, animated: Bool = true) {
        parentView.window!.addSubview(self.view)
        
        if animated {
            self.showAnimate()
        }
    }
    
    
    func showAnimate() {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: [], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.alertView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alertView.layer.position = self.view.center
            
            }, completion: nil)
    }
    
    func removeAnimate() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.fromValue = NSValue(cgPoint: view.center)
        animation.toValue = NSValue(cgPoint: centerOfTheStart)
        animation.duration = 0.3
        self.alertView.layer.add(animation, forKey: nil)
        
        UIView.animate(withDuration: 0.3, animations:  {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
            self.alertView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion:  { Void in
                self.view.removeFromSuperview()
        })
        
        UIView.animate(withDuration: 0.1, delay: 0.2, options: [], animations: {
            self.alertView.alpha = 0
            }, completion: nil)
    }
    
    
    @IBOutlet weak var subscribe: UIButton!
    @IBAction func subscribe(_ sender: UIButton) {
        appearanceModel.title = "Wait..."
        sender.isEnabled = false
        appearanceModel.sendRequest(linkToSubscribe) { (answer) in
            switch answer {
            case true:
                self.appearanceModel.title = "Done!"
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.removeAnimate()
                    if let vcChampshReferance = self.referanceVC as? ListChampsh {
                        vcChampshReferance.isFollow = self.isFollowed ? false : true
                    } else if let vcStreamsReferance = self.referanceVC as? ListStreams {
                        vcStreamsReferance.isFollow = self.isFollowed ? false : true
                    } else if let vcStreamsReferance = self.referanceVC as? ListChampshPad {
                        vcStreamsReferance.isFollow = self.isFollowed ? false : true
                    } else if let vcStreamsReferance = self.referanceVC as? ListStreamsPad {
                        vcStreamsReferance.isFollow = self.isFollowed ? false : true
                    }
                })
                
            case false:
                sender.isEnabled = true
                self.alertMassage.text = "Sorry, you have a problems with internet connection"
                self.appearanceModel.title = "Try again!"
            }
            
        }
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard event?.touches(for: alertView) == nil else {
            return
        }
        removeAnimate()
    }
    
}
