//
//  Player.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 21.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class Player: AVPlayerViewController {
    
    let volumeView = MPVolumeView()
    let audioSession = AVAudioSession.sharedInstance()
    var linksArray = [String]()
    
    var avPlayer: AVPlayer?
    var tapCount = 1
    
    @IBOutlet weak var unavailableStreamImg: UIImageView!
    @IBOutlet weak var unavailbleStream: UILabel!
    @IBOutlet var playbackCustom: UIView!
    @IBOutlet weak var viewVithControls: UIView!
    @IBOutlet weak var activInd: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.setStatusBarHidden(true, with: .fade)
        self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        streamSettings()
        volumeSettings()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playbackCustom.frame = view.bounds
        let tapGestureReconizer = UITapGestureRecognizer(target: self, action: #selector(Player.tap))
        self.playbackCustom.addGestureRecognizer(tapGestureReconizer)
    }
    
    
    ///Audio Controls
    
    @IBOutlet weak var sliderVolume: UISlider!
    @IBAction func sliderVolume(_ sender: UISlider) {
        let selectedValue = Float(sender.value)
        volumeView.alpha = 0
        for view in volumeView.subviews {
            if (NSStringFromClass(view.classForCoder) == "MPVolumeSlider") {
                let slider = view as! UISlider
                slider.setValue(selectedValue, animated: true)
            }
        }
        
    }
    
    func listenVolumeButton() {
        try! audioSession.setActive(true)
        setAudioSlider()
        audioSession.addObserver(self, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "outputVolume" {
            setAudioSlider()
        }
    }
    
    func setAudioSlider() {
        let volume = AVAudioSession.sharedInstance().outputVolume
        sliderVolume.value = volume
    }
    
    
    func tap() {
        tapCount += 1
        if tapCount % 2 == 0 {
            UIView.animate(withDuration: 0.3, animations: {
                self.viewVithControls.alpha = 1
                self.back.alpha = 1
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.viewVithControls.alpha = 0
                self.back.alpha = 0
                self.qualityView.alpha = 0
            })
        }
    }
    
    @IBOutlet weak var back: UIButton!
    
    @IBAction func back(_ sender: AnyObject) {
        avPlayer = nil
        player = nil
        kMainQueue.async {
            self.dismiss(animated: true, completion: {
            })
        }
        
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
    }
    
    @IBOutlet weak var playNpause: UIButton!
    @IBAction func playNpause(_ sender: AnyObject) {
        checkPlayer()
    }
    
    @IBAction func settings(_ sender: AnyObject) {
        UIView.animate(withDuration: 0.3, animations: {
            if self.qualityView.alpha == 0 {
                self.qualityView.alpha = 1
            } else {
                self.qualityView.alpha = 0
            }
        }) 
    }
    
    /////Quality
    @IBOutlet weak var qualityView: UIView!
    
    @IBOutlet var QButt: [UIButton]!
    
    @IBAction func sourceQ(_ sender: UIButton) {
        tap()
        playMovie(4)
        
    }
    
    @IBAction func highQ(_ sender: UIButton) {
        tap()
        playMovie(3)
    }
    
    
    @IBAction func mediumQ(_ sender: UIButton) {
        tap()
        playMovie(2)
    }
    
    
    @IBAction func lowQ(_ sender: UIButton) {
        tap()
        playMovie(1)
    }
    
    
    @IBAction func mobileQ(_ sender: UIButton) {
        tap()
        playMovie(0)
    }
    
    func playMovie(_ numb: Int) {
        activInd.startAnimating()
        self.player?.pause()
        for i in QButt {
            i.backgroundColor = UIColor.black
            i.isUserInteractionEnabled = true
        }
        QButt[numb].backgroundColor = UIColor(red: 233/255, green: 159/255, blue: 34/255, alpha: 1)
        QButt[numb].isUserInteractionEnabled = false
        if let movieurl = URL(string: linksArray[numb]) {
            //Check if stream is available
            kBgQ1.async(execute: { () -> Void in
                if (try? Data(contentsOf: movieurl)) != nil {
                    kMainQueue.async(execute: { () -> Void in
                        self.unavailbleStream.alpha = 0
                        self.unavailableStreamImg.alpha = 0
                        self.avPlayer = AVPlayer(url: movieurl)
                        self.player = self.avPlayer
                        self.checkPlayer()
                        self.activInd.stopAnimating()
                    })
                } else {
                        self.showThatStreamIsUnavailable()
                }
            })
        }
    }
    
    
    
    func checkPlayer() {
        if player?.rate == 0 && player != nil {
            rewindMovie()
            playNpause.setBackgroundImage(UIImage(named: "pause"), for: UIControlState())
        } else if player?.rate == 1 && player != nil {
            player?.pause()
            playNpause.setBackgroundImage(UIImage(named: "play"), for: UIControlState())
        }
    }
    
    func rewindMovie() {
        player!.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
        self.player?.play()
    }
    
    func streamSettings() {
        for i in 0..<linksArray.count {
            linksArray[i] = linksArray[i].toUTF8
            print(linksArray[i])
            if linksArray[i] == "nil" {
                QButt[i].setTitleColor(UIColor.gray, for: UIControlState())
                QButt[i].isEnabled = false
            }
        }
        if linksArray[2] != "nil" {
            playMovie(2)
        } else {
            for index in 0..<linksArray.count {
                if linksArray[index] != "nil" {
                    playMovie(index)
                    break
                } else if index == linksArray.count - 1 && linksArray[index] == "nil" {
                    showThatStreamIsUnavailable()
                }
            }
        }
    }
    
    func showThatStreamIsUnavailable() {
        self.unavailbleStream.alpha = 1
        self.unavailableStreamImg.alpha = 1
        self.activInd.stopAnimating()
    }
    
    func volumeSettings() {
        listenVolumeButton()
        view.addSubview(playbackCustom)
        sliderVolume.setThumbImage(UIImage(named: "vollume-pimpa"), for: UIControlState())
        let volumeView: MPVolumeView = MPVolumeView(frame: CGRect(x: -1000, y: -1000, width: 100, height: 100))
        volumeView.sizeToFit()
        self.view.addSubview(volumeView)
        // Play with silent mode
        do { try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            do{ try AVAudioSession.sharedInstance().setActive(true) }
            catch _ as NSError { } }
        catch _ as NSError { }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        audioSession.removeObserver(self, forKeyPath: "outputVolume")
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }
    
    
    
}










