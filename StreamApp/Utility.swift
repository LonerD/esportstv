//
//  StringUtility.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 25.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var toUTF8: String {
        return decoder(self)
    }
    
    var clearSpace: String {
        return celar(self)
    }
    
    
    func decoder(_ string: String) -> String {
        var utf = string
        utf = utf.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        utf = utf.replacingOccurrences(of: "%0A%20", with:"")
        utf = utf.replacingOccurrences(of: "%20", with:"")
        utf = utf.replacingOccurrences(of: "/ampersand/", with:"&")
        
        return utf
    }
    
    func celar(_ string: String) -> String {
        var clearStr = string
        clearStr = clearStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return clearStr
    }
    
    
    
}



extension UIColor {
    func randomColor(_ existColors: [UIColor]) -> UIColor {
        // Number of colors
         var hexColors = ["#CC00FF", "#9900FF", "#6633CC", "#6600FF", "#3300FF", "#3366FF", "#0066FF", "#0099FF", "#00CCFF", "#33FFFF", "#CC00CC", "#CC66FF", "#3333FF"]
        
        for color in hexColors {
            for existColor in existColors {
                if UIColor(hexString: color) == existColor {
                    hexColors.remove(at: hexColors.index(of: color)!)
                }
            }
        }
        
        if hexColors.count == 0 {
            hexColors = ["#CC00FF", "#9900FF", "#6633CC", "#6600FF", "#3300FF", "#3366FF", "#0066FF", "#0099FF", "#00CCFF", "#33FFFF", "#CC00CC", "#CC66FF", "#3333FF"]
        }
        
        let randomNumber = Int(arc4random_uniform(UInt32(hexColors.count)))
        let colorHexString = hexColors[randomNumber]
        
        let color = UIColor(hexString: colorHexString)
        
        
        return color
        
        
    }
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (r, g, b) = ((int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (r, g, b) = (int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (r, g, b) = (int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (r, g, b) = (1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: 1)
    }
    
    
}


extension Data {
    func hexString() -> String {
        return self.reduce("") { string, byte in
            string + String(format: "%02X", byte)
        }
    }
}


extension String {
    var cestToCurrentTime: (String?, String?) {
        let q = self.dateConverter(date: self)
        return q
    }

    func dateConverter(date: String) -> (String?, String?) {
        
        let formater = DateFormatter()
        
        
        formater.dateFormat = "dd.MM.yyyy HH:mm"
        
        formater.timeZone = TimeZone(abbreviation: "CEST")
        
        let fullDate: Date? = formater.date(from: date)?.addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
        
        guard fullDate != nil  else {
            return (nil, nil)
        }
        
        
        formater.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        formater.dateFormat = "dd.MM.yyyy"
        
        let date = formater.string(from: fullDate!)
        
        formater.dateFormat = "HH:mm"
        
        let time = formater.string(from: fullDate!)
        return (time, date)
        
    }
}

