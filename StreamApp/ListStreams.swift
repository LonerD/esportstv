//
//  listStreams.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 22.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class ListStreams: UITableViewController, XMLParserDelegate {
    
    var alertNotifyView: NotificationViewController?
    
    var parser: XMLParser!
    weak var adView: AdvManager!
    
    
    //Notification Block
    var parentVC: ListChampsh?
    var index: Int!
    var isFollow = Bool() {
        didSet {
            guard parentVC != nil && index != nil else {
                return
            }
            parentVC!.eventMembers[index]?.subscribeManager(isFollow ? 1 : 0)
            setupNavBar()
        }
    }
    
    //AdMob Service
    var advContentView = [UIView]()
    
    let downloadImage = ImageDownloader()
    
    
    ///Data From Server
    struct streamsData {
        var titleOfTheStream: String
        var languageOfTheStream: String
        var imageOfTheStreams: String
        var streamLinks: String
    }
    
    var streamMembers = [streamsData?]()
    
    ///parse String from Server
    var linkXMLStreams = String()
    var navigationTitle = String()
    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressView.setProgress(0.3, animated: true)
        title = navigationTitle
        
        parse(linkXMLStreams)
        
    }
    
    func XMLParserError(_ error: String) {
        kMainQueue.async {
            self.showUnavailability("There are problems with the connection to the server")
        }
    }
    
    
    
    func setupNavBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.backgroundColor = UIColor.init(patternImage: UIImage(named: "backgroundTV")!)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: (isFollow ? UIImage(named: "bellIsFollowed")! : UIImage(named: "bellIsUnfollowed")), style: .plain, target: self, action: #selector(ListStreams.notificationSubscribe))
    }
    
    func notificationSubscribe() {
        alertNotifyView = NotificationViewController(nibName: "NotificationView", bundle: nil)
        
        alertNotifyView?.isFollowed = isFollow
        alertNotifyView?.linkToSubscribe = linkXMLStreams
        
        if let targetView = navigationItem.rightBarButtonItem?.value(forKey: "view") {
            alertNotifyView?.centerOfTheStart = (targetView as AnyObject).center
        }
        
        alertNotifyView?.referanceVC = self
        alertNotifyView?.textToAlert = "Do you want to " + (isFollow ? "unfollow " : "follow ") + navigationTitle + "?"
        
        alertNotifyView!.view.frame = self.view.frame
        alertNotifyView!.showInView(self.view)
        
        
    }
    
    func parse(_ linkToParse: String) {
        parser = XMLParser(url: linkToParse)
        parser.delegate = self
        
        parser.parse {
            if self.parser.objects.count != 0 {
                for i in 0..<self.parser.objects.count {
                    self.streamMembers.append(streamsData(titleOfTheStream: self.parser.objects[i]["title"]!.toUTF8,
                        languageOfTheStream: self.parser.objects[i]["language"]!.clearSpace,
                        imageOfTheStreams: self.parser.objects[i]["img"]!.toUTF8,
                        streamLinks: self.parser.objects[i]["StreamLink"]!))
                }
                    self.createAdvCells()
                    self.tableView.reloadData()
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        self.progressView.setProgress(1, animated: true)
                    })
                    ///progressView
                    UIView.animate(withDuration: 0.3, delay: 0.3, options: [], animations: {
                        self.tableView.contentInset.top = 61
                        self.progressView.alpha = 0
                        }, completion: nil)
            } else {
                self.showUnavailability("There are no streams available yet")
            }
        }
    }
    
    func showUnavailability(_ text: String) {
        
        let errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 64), textToShow: text)
        self.view.addSubview(errorView)
        
        self.progressView.setProgress(0.8, animated: true)
        
        self.tableView.isScrollEnabled = false
        UIView.animate(withDuration: 1, animations: {
            self.progressView.setProgress(0, animated: true)
        }) 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "player" {
            let destVC = segue.destination as! Player
            if let indexPath = sender as? IndexPath {
                destVC.linksArray = (streamMembers[indexPath.row]!.streamLinks).components(separatedBy: ", ")
            }
        }
    }
}



//Implementing Advertisenemts
extension ListStreams {
    func createAdvCells() {
        let visibleCells = 6
        
        let random = self.streamMembers.count < visibleCells ? Int(arc4random_uniform(UInt32(self.streamMembers.count))) : 3
        
        self.streamMembers.insert(nil, at: random)
        
        for index in random...self.streamMembers.count {
            
            if index != 0 && index % visibleCells == 0 && streamMembers.indices.contains(index + random)  {
                
                self.streamMembers.insert(nil, at: index + random)
            }
        }
    }
    
}


// TableView DataSource

extension ListStreams {
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return streamMembers.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellStreams", for: indexPath) as! CustomCellStreams
        cell.selectionStyle = .default
        for subview in cell.subviews {
            if subview.tag == 1 {
                subview.removeFromSuperview()
            }
        }
        
        guard streamMembers[indexPath.row] != nil else {
            cell.selectionStyle = .none
            var count = -1
            for (index, element) in streamMembers.enumerated() {
                if element == nil {
                    count += 1
                    if index == indexPath.row {
                        if !advContentView.indices.contains(count) {
                            adView = AdvManager(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: tableView.frame.height / 6), viewController: self)
                            adView.translatesAutoresizingMaskIntoConstraints = false
                            adView.center.x = cell.center.x
                            adView.tag = 1
                            advContentView.append(adView)
                            
                        }
                        cell.addSubview(advContentView[count])
                        
                        let views = ["advView" : advContentView[count], "cell" : cell.contentView]
                        
                        cell.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[advView(==cell)]|", options: [], metrics: nil, views: views))
                        
                        cell.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[advView]|", options: [], metrics: nil, views: views))
                    }
                }
            }
            return cell
        }
        
        cell.imageStreams.image = UIImage(named: "loading")
        
        cell.languageStream.text = streamMembers[indexPath.row]!.languageOfTheStream
        cell.languageStream.highlightedTextColor = UIColor(red: 233/255, green: 159/255, blue: 34/255, alpha: 1)
        cell.titleStreams.text = streamMembers[indexPath.row]!.titleOfTheStream
        cell.imageStreams.image = UIImage(named: "loading")
        
        let urlString = streamMembers[indexPath.row]!.imageOfTheStreams
        
        let downloadImage = ImageDownloader()
        
        downloadImage.downloadImages(urlString, ifPhotoAvailable: { (image) in
            cell.imageStreams.image = image
        }) { (image) in
            cell.imageStreams.alpha = 0
            cell.imageStreams.image = image
            UIView.animate(withDuration: 0.3, animations: {
                cell.imageStreams.alpha = 1
            })
        }
        
        
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard streamMembers[indexPath.row] != nil else {
            return
        }
        performSegue(withIdentifier: "player", sender: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    
    
    
    
    
    
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            cell.alpha = 1
        }) 
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height / 6
    }
}



