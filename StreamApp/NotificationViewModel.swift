//
//  NotificationViewModel.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 29.08.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit


protocol NotificationAppearance {
    func sendRequest(_ url: String, answer: @escaping (Bool) -> ())
    var title: String {get set}
    var color: UIColor {get set}
}

struct Subscribe: NotificationAppearance {

    var title = "Follow"
    var color = UIColor(hexString: "FFA734")
    
     internal func sendRequest(_ url: String, answer: @escaping (Bool) -> ()) {
        let manager = RequestManager(urlForRequest: host + "/subscription-devices?", parametrs: ["link" : url, "token" : token!, "status" : "1"], reqType: .Get)
        manager.requestTask { (response, error) in
            guard error == nil else {
                answer(false)
                print(response)
                return
            }
            print(response)
            answer(true)
            
        }
    }
    
}


struct Unsubscribe: NotificationAppearance {



    var title = "Unfollow"
    var color = UIColor(hexString: "D7001E")
    internal func sendRequest(_ url: String, answer: @escaping (Bool) -> ()) {
        
        let manager = RequestManager(urlForRequest: host + "/subscription-devices?", parametrs: ["link" : url, "token" : token!, "status" : "0"], reqType: .Get)
        
        manager.requestTask { (response, error) in
            guard error == nil else {
                answer(false)
                return
            }
            print(response)
            answer(true)
        }
        
    }
    
}


struct NotifyCoordinator {
    fileprivate(set) var coordinatorDidUpdateStateModelBlock: (_ model: NotificationAppearance) -> ()
    
    init(coordinatorDidUpdateStateModelBlock: @escaping (_ model: NotificationAppearance) -> ()) {
        self.coordinatorDidUpdateStateModelBlock = coordinatorDidUpdateStateModelBlock
    }
    
    func setModel(_ isFollowed: Bool) {
        switch isFollowed {
        case true:
            coordinatorDidUpdateStateModelBlock(Unsubscribe())
        case false:
            coordinatorDidUpdateStateModelBlock(Subscribe())
        }
    }
    
    
}

