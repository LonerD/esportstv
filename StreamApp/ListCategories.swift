//
//  ViewController.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 21.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.

import UIKit

enum Following {
    case follow
    case unfollow
}

class ListCategories: UIViewController, UIPageViewControllerDataSource, UITableViewDelegate, UITableViewDataSource, XMLParserDelegate {
    
    
    //Parser Links
    let categoriesXMLLink = host + "/Categories"
    let sliderXMLLink = host + "/Slider"
    
    
    var pageViewController: UIPageViewController! { didSet { self.pageViewController.dataSource = self } }
    var startViewController = UIViewController()
    
    @IBOutlet weak var tableViewHeading: UITableView!
    
    var parser: XMLParser!
    
    fileprivate let downloadImage = ImageDownloader()
    
    
    // Structs
    struct categoriesData {
        var headingsOfTheGame: String
        var XMLLinkOfTheGame: String
        var title: String
        var subscribe: Following = .unfollow
        
        mutating func subscribeManager(_ type: String) {
            switch type {
            case "0": subscribe = .unfollow
            default: subscribe = .follow
            }
        }
        
    }
    
    struct sliderData {
        var pageImage: String
        var pageStreamLink: String
        var pageAdvLink: String
    }
    
    var categotiesMembers = [categoriesData]()
    
    var sliderMembers = [sliderData(pageImage: "", pageStreamLink: "", pageAdvLink: "")]
    
    
    fileprivate var timer: Timer?
    
    //startPageProgress
    fileprivate var progressNumber = 0
    fileprivate var statusOfTheEnd = 0
    
    fileprivate var actualPage = -1 {
        didSet {
            if sliderMembers.count > 1 {
                if timer != nil {
                    timer!.invalidate()
                }
                self.timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(ListCategories.restartAction), userInfo: nil, repeats: false)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callStarVC()
        setPageView()
        
        if token != nil {
            parseCatigories()
        }
    }
    
    
    
    
    func XMLParserError(_ error: String) {
        notifCentr.post(name: Notification.Name(rawValue: "parseError"), object: nil)
    }
    
    func restartAction() {
        var startVC: ContentViewController
        var viewControllers: NSArray
        actualPage += 1
        if actualPage < sliderMembers.count {
            startVC = self.viewControllerAtIndex(actualPage)
            viewControllers = NSArray(object: startVC)
            self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .forward, animated: true, completion: nil)
        } else {
            actualPage = 0
            startVC = self.viewControllerAtIndex(actualPage)
            viewControllers = NSArray(object: startVC)
            self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    func viewControllerAtIndex(_ index: Int) -> ContentViewController    {
        if self.sliderMembers.count == 0 || index >= self.sliderMembers.count {
            return ContentViewController()
        }
        
        let vc: ContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
        //////  Encode link to UTF8
        self.sliderMembers[index].pageImage = (sliderMembers[index].pageImage)
        sliderMembers[index].pageAdvLink = (sliderMembers[index].pageAdvLink)
        
        vc.imageFile = self.sliderMembers[index].pageImage
        vc.linksStream = self.sliderMembers[index].pageStreamLink.components(separatedBy: ", ")
        vc.advLink = self.sliderMembers[index].pageAdvLink
        vc.pageIndex = index
        
        return vc
        
    }
    
    
    
    // MARK: - Page View Controller Data Source
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! ContentViewController
        var index = vc.pageIndex as Int
        actualPage = index
        if index == 0 || index == NSNotFound {
            return nil
        }
        index -= 1
        return self.viewControllerAtIndex(index)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! ContentViewController
        var index = vc.pageIndex as Int
        actualPage = index
        if index == NSNotFound {
            return nil
        }
        index += 1
        if index == self.sliderMembers.count {
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    //MARK: TableView Settings
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categotiesMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewHeading.dequeueReusableCell(withIdentifier: "Cell")! as! CustomCellHeadings
        ////EncodeUTF8

        let urlString = categotiesMembers[indexPath.row].headingsOfTheGame
        
        downloadImage(urlString) { (image) in
            cell.imageNamesOfTheHeadings.image = image
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return pageViewController.view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let heigthForHeader = tableViewHeading.frame.height / 2 - 44
        if categotiesMembers.count <= 4 {
            let dynamicCellsFrame = (tableViewHeading.frame.height - heigthForHeader) / CGFloat(categotiesMembers.count)
            return dynamicCellsFrame
        } else {
            let staticCellsFrame = (tableViewHeading.frame.height - heigthForHeader) / 4
            return staticCellsFrame
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let heigthForHeader = tableViewHeading.frame.height / 2 - 44
        return heigthForHeader
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableViewHeading.deselectRow(at: indexPath, animated: true)
    }
    
    ////parse info for firstView
    
    func parseCatigories() {
        guard token != nil else {
            return
        }
        
        parser = XMLParser(url: categoriesXMLLink)
        parser.delegate = self
        parser.parse {
            
            for i in 0..<self.parser.objects.count {
                self.categotiesMembers.append(categoriesData(
                    headingsOfTheGame: self.parser.objects[i]["img"]!.toUTF8,
                    XMLLinkOfTheGame: self.parser.objects[i]["linkXML"]!.toUTF8,
                    title: self.parser.objects[i]["title"]!.toUTF8, subscribe: .unfollow))
                    self.downloadImage(self.categotiesMembers[i].headingsOfTheGame, image: nil)
                self.categotiesMembers[i].subscribeManager(self.parser!.objects[i]["subscribe"]!.toUTF8)
            }
                self.statusOfTheEnd = 100 / self.categotiesMembers.count
                self.parseSlider(self.sliderXMLLink)
        }
    }
    
    
    fileprivate func parseSlider(_ linkToParse: String) {
        self.parser = XMLParser(url: linkToParse)
        
        self.parser!.parse { 
            self.sliderMembers = []
            for i in 0..<self.parser.objects.count {
                self.sliderMembers.append(sliderData(
                    pageImage: self.parser.objects[i]["img"]!.toUTF8,
                    pageStreamLink: self.parser.objects[i]["linkStream"]!,
                    pageAdvLink: self.parser.objects[i]["linkAdv"]!.toUTF8))
                
            }
                self.restartAction()
        }
        
    }
    
    func downloadImage(_ imageLink: String, image: ((UIImage) -> ())?) {
        var progress = Float()
        
        downloadImage.downloadImages(imageLink, ifPhotoAvailable: { (downloadedImage) in
            image?(downloadedImage)
            }) { [unowned self] (downloadedImage) in
                guard self.progressNumber < self.categotiesMembers.count else {
                    image?(downloadedImage!)
                    return
                }
                // Start display ProgressBar
                self.progressNumber += 1
                progress = Float(self.statusOfTheEnd * self.progressNumber) / 100
                let reqiredUserInf = ["progress" : progress]
                NotificationCenter.default.post(name: Notification.Name(rawValue: "passDataInView"), object: nil, userInfo: reqiredUserInf)
                if progress >= 0.99 {
                    Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ListCategories.deleteStartVC), userInfo: nil, repeats: false)
                    self.tableViewHeading.reloadData()
                }
                image?(downloadedImage!)
        }
    }
    
    //////SetupViewDidLoad
    fileprivate func setPageView() {
        pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! UIPageViewController
        pageViewController.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundTV")!)
        
        tableViewHeading.backgroundColor = UIColor.init(patternImage: UIImage(named: "backgroundTV")!)
        self.tableViewHeading.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
        ///setNavigationBar
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 20))
        let titleImageView = UIImageView(image: UIImage(named: "logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)
        titleView.addSubview(titleImageView)
        navigationItem.titleView = titleView
        notifCentr.addObserver(self, selector: #selector(ListCategories.parseCatigories), name: NSNotification.Name(rawValue: "reloadCategories"), object: nil)
        
    }
    
    func callStarVC() {
        startViewController = self.storyboard?.instantiateViewController(withIdentifier: "startScreen") as! StartScreen
        let currentVindow = UIApplication.shared.keyWindow
        currentVindow?.addSubview(startViewController.view)
    }
    
    func deleteStartVC() {
        self.startViewController.view.removeFromSuperview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notifCentr.removeObserver(self, name: NSNotification.Name(rawValue: "reloadCategories"), object: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showChmpsh" {
            let destVC = segue.destination as! ListChampsh
            if let cell = sender as? UITableViewCell {
                if let indexPath = tableViewHeading.indexPath(for: cell) {
                    destVC.XMLStringChmpsh = categotiesMembers[indexPath.row].XMLLinkOfTheGame
                    destVC.navigationTitle = categotiesMembers[indexPath.row].title
                    destVC.index = indexPath.row
                    destVC.parentVC = self
                    switch categotiesMembers[indexPath.row].subscribe {
                    case .follow: destVC.isFollow = true
                    case .unfollow: destVC.isFollow = false
                    }
                }
            }
        }
    }
    
}


