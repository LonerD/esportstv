//
//  ListStreamsPad.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 12.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class ListStreamsPad: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    //CollectionView
    @IBOutlet weak var collectionViewStreams: UICollectionView!
    var width: CGFloat!
    var height: CGFloat!
    
    //Activity Indicator    
    var activityIndicator = FPActivityLoader()
    
    
    //Notification Block
    var alertNotifyView: NotificationViewController?
    weak var parentVC: ListChampshPad?
    
    var index: Int!
    var isFollow = Bool() {
        didSet {
            guard parentVC != nil && index != nil else {
                return
            }
            parentVC!.eventMembers[index]?.subscribeManager(isFollow ? 1 : 0)
            setSubscribeImage()
        }
    }

    
    ///Data From Server
    
    var parser: XMLParser!
    let downloadImage = ImageDownloader()
    
    struct streamsData {
        var titleOfTheStream: String
        var languageOfTheStream: String
        var imageOfTheStreams: String
        var streamLinks: String
    }
    
    var streamMembers = [streamsData?]()
    
    
    //Parse link
    var linkXMLToParse = String()
    
    var subtitle = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createActivityIndicator()
        parse(linkXMLToParse)
        setSubscribeImage()
    }

    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        prepareView()
    }
    

    @IBAction func backToCategories(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func setCollectionViewLayoutDirection(_ direction: Direction) {
        
        switch direction {
        case .hor :
            width = (view.bounds.size.width - 90 * 2 - 2 * 8) / 4
            height = (view.bounds.size.width - 30 - 10 * 4) / 3.8
        case .ver :
            width = (view.bounds.size.width - 90 * 2 - 2 * 4) / 3
            height = (view.bounds.size.width - 20 * 5) / 2.8
        }
        kMainQueue.async {
            self.collectionViewStreams.collectionViewLayout.invalidateLayout()
        }
    }
    
    
    
    //CollectionView Data Source
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {

            return CGSize(width: width, height: height)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return streamMembers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewStreams.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ListStreamsCVCellPad
        
        cell.imageStreams.image = UIImage(named: "loadingPad")
        cell.nameOfTheStream.text = streamMembers[indexPath.row]?.titleOfTheStream
        
        cell.languageOfTheStream.text = streamMembers[indexPath.row]?.languageOfTheStream
        cell.languageOfTheStream.highlightedTextColor = UIColor(red: 233/255, green: 159/255, blue: 34/255, alpha: 1)
        
        let urlString = streamMembers[indexPath.row]!.imageOfTheStreams
        
        downloadImage.downloadImages(urlString, ifPhotoAvailable: { (image) in
            cell.imageStreams.image = image
        }) { (image) in
            cell.imageStreams.alpha = 0
            cell.imageStreams.image = image
            UIView.animate(withDuration: 0.3, animations: {
                cell.imageStreams.alpha = 1
            })
        }
        
        
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionViewStreams.cellForItem(at: indexPath)
        UIView.animate(withDuration: 0.2, animations: {
            cell?.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        })
        return true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionViewStreams.cellForItem(at: indexPath)
        UIView.animate(withDuration: 0.2, animations: {
            cell?.transform = CGAffineTransform.identity
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "player", sender: indexPath)
        collectionViewStreams.deselectItem(at: indexPath, animated: false)
    }
    
    
    @IBOutlet weak var subscriber: UIButton!
    
    @IBAction func subscriber(_ sender: AnyObject) {
        notificationSubscribe()
    }
    
    func setSubscribeImage() {
        let image = isFollow ? UIImage(named: "bellIsFollowed") : UIImage(named: "bellIsUnfollowed")
        if subscriber != nil {
            subscriber.setImage(image, for: .normal)
        }
    }
    
    
    func prepareView() {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundTV")!)
        
        subscriber.layer.shadowColor = UIColor.black.cgColor
        subscriber.layer.shadowOpacity = 0.5
        subscriber.layer.shadowRadius = 3
        subscriber.layer.shadowOffset = CGSize(width: 0, height: 5)
        subscriber.layer.masksToBounds = false
        
        let oritntation = view.checkOrientation()
        setCollectionViewLayoutDirection(oritntation)
       
    }
    
    func createActivityIndicator() {
        let viewWidth = view.frame.size.height / 12
        activityIndicator.circleTime = 2
        activityIndicator.frame.size = CGSize(width: viewWidth, height: viewWidth)
        activityIndicator.strokeColor = UIColor(red: 255/255, green: 167/255, blue: 52/255, alpha: 1)
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.animating = true
    }
    
    func UIErrorDisplay(text toDisplay: String) {
        let viewWithErrorText = ErrorView(frame: collectionViewStreams.bounds, textToShow: toDisplay)
        self.activityIndicator.animating = false
        collectionViewStreams.addSubview(viewWithErrorText)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "player" {
            let destVC = segue.destination as! Player
            if let indexPath = sender as? IndexPath {
                destVC.linksArray = (streamMembers[indexPath.row]!.streamLinks).components(separatedBy: ", ")
            }
        }
    }
    
    
    
}

extension ListStreamsPad: XMLParserDelegate {
    func XMLParserError(_ error: String) {
        kMainQueue.async {
            self.UIErrorDisplay(text: "There are problems with the connection to the server")
        }
    }
    
    func parse(_ linkToParse: String) {
        parser = XMLParser(url: linkToParse)
        parser.delegate = self
        
        parser.parse {
            kMainQueue.async {
                self.activityIndicator.animating = false
            }
            
            
                guard self.parser.objects.count != 0 else {
                    self.UIErrorDisplay(text: "There are no streams available yet")
                    return
                }
            
                for i in 0..<self.parser.objects.count {
                    self.streamMembers.append(streamsData(titleOfTheStream: self.parser.objects[i]["title"]!.toUTF8,
                    languageOfTheStream: self.parser.objects[i]["language"]!.clearSpace,
                    imageOfTheStreams: self.parser.objects[i]["imgPad"]!.toUTF8,
                    streamLinks: self.parser.objects[i]["StreamLink"]!))
                }
            
                self.collectionViewStreams.reloadData()
        
        }
        
    }

    
    
}

extension ListStreamsPad {
    func notificationSubscribe() {
        alertNotifyView = NotificationViewController(nibName: "NotificationView", bundle: nil)
        
        alertNotifyView?.isFollowed = isFollow
        
        alertNotifyView?.centerOfTheStart = subscriber.center
        
        alertNotifyView?.referanceVC = self
        alertNotifyView?.linkToSubscribe = linkXMLToParse
        alertNotifyView?.textToAlert = "Do you want to " + (isFollow ? "unfollow " : "follow ") + subtitle + "?"
        
        
        alertNotifyView!.view.frame = self.view.frame
        alertNotifyView!.showInView(self.view)
        
    }
}
