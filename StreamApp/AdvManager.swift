//
//  AdvManager.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 02.08.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdvManager: UIView, GADNativeExpressAdViewDelegate {
    
    var nativeExpressAdView: GADNativeExpressAdView!
    let activityIndAdv = UIActivityIndicatorView()
    
    var viewController: UIViewController!
    
    init(frame: CGRect, viewController: UIViewController) {
        super.init(frame: frame)
        self.viewController = viewController
        setUpAdv()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func nativeExpressAdViewWillPresentScreen(_ nativeExpressAdView: GADNativeExpressAdView!) {
        print("userClicked")
    }
    
    func nativeExpressAdViewDidReceiveAd(_ nativeExpressAdView: GADNativeExpressAdView!) {
        print("recieve")
        activityIndAdv.stopAnimating()
    }
    
    func nativeExpressAdView(_ nativeExpressAdView: GADNativeExpressAdView!, didFailToReceiveAdWithError error: GADRequestError!) {
        print(error.localizedDescription)
    }
    
    func setUpAdv() {
        
        self.nativeExpressAdView = GADNativeExpressAdView()
        
        self.nativeExpressAdView.isAutoloadEnabled = true
        nativeExpressAdView.delegate = self
        
        //ADMobSettings
        
        
        nativeExpressAdView.adUnitID = "ca-app-pub-7895107980112975/5181152234"
        
        nativeExpressAdView.rootViewController = self.viewController
        nativeExpressAdView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 100)
        nativeExpressAdView.adSize = GADAdSize(size: CGSize(width: self.frame.width, height: 100), flags: 0)
        self.advRequest()
        self.createViewWithAd()
    }
    
    func advRequest() {
        let request = GADRequest()
        
        self.nativeExpressAdView.load(request)
    }
    
    
    func createViewWithAd() {
        
        self.backgroundColor = UIColor(red: 20/255, green: 24/255, blue: 31/255, alpha: 1)
        nativeExpressAdView.translatesAutoresizingMaskIntoConstraints = false
        nativeExpressAdView.center.y = self.center.y
        
        self.addSubview(nativeExpressAdView)
        
        let views = ["nativeExpressAdView" : nativeExpressAdView, "view" : self] as [String : Any]
        
        let topPenddind = self.frame.height / 2 - nativeExpressAdView.frame.height / 2
        
        let metrics =  ["top" : topPenddind > 0 ? topPenddind : 0]
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[nativeExpressAdView(==view)]|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-top-[nativeExpressAdView(100)]|", options: [], metrics: metrics, views: views))
        
        activityIndAdv.activityIndicatorViewStyle = .whiteLarge
        activityIndAdv.hidesWhenStopped = true
        activityIndAdv.startAnimating()
        activityIndAdv.center = self.center
        self.addSubview(activityIndAdv)
        
    }
    
    
    deinit {
        print("wooow")
    }
}
