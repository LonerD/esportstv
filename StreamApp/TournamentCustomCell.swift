//
//  TournamentCustomCell.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 18.07.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class TournamentCustomCell: UICollectionViewCell {
    
    
    @IBOutlet weak var timeTournament: UILabel!
    
    @IBOutlet weak var dateTournament: UILabel!
    
    @IBOutlet weak var firstCommandTitle: UILabel!
    
    @IBOutlet weak var secondCommandTitle: UILabel!
    
    @IBOutlet weak var bestOf: UILabel!
    
   
    @IBOutlet weak var firstCommandImg: UIImageView!
    
    
    @IBOutlet weak var secondCommandImg: UIImageView!
    
    
    @IBOutlet weak var firstCommandScore: UILabel!
    
    @IBOutlet weak var secondCommandScore: UILabel!
    
    @IBOutlet weak var timeInfoSubview: UIView!
    
    
}
