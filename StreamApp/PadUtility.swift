//
//  PadUtility.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 21.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

enum Direction {
    case hor
    case ver
}

extension UIView {
    func checkOrientation() -> Direction {
    let orientation = UIApplication.shared.statusBarOrientation
    
    switch orientation {
    case .portrait: return .ver
    case .landscapeRight: return .hor
    default: return .ver
    }

    }
}
