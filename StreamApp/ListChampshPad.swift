//
//  ListChampshPad.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 12.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class ListChampshPad: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate {
    
    var parser: XMLParser!
    var linkXMLToParse = String()
    
    //Activity Indicatior
    var activityIndicator = FPActivityLoader()
    

    //Notification Block
    var alertNotifyView: NotificationViewController?
    weak var parentVC: ListCategoriesPad?
    
    var index: Int!
    var isFollow = Bool() {
        didSet {
            guard parentVC != nil && index != nil else {
                return
            }
            parentVC!.categotiesMembers[index].subscribeManager(isFollow ? "1" : "0")
            setSubscribeImage()
        }
    }
    
    ///Data From Server
    
    let downloadImage = ImageDownloader()
    
    struct eventsData {
        var events: String
        var imageLink: String
        var prizeFund: String
        var eventStreamsXML: String
        
        var subtitle: String
        
        var startDate: String
        var endDate: String
        var description: String
        var linkXMLTournament: String
        
        var subscribe: Following = .unfollow
        var isLive: Bool
        
        mutating func subscribeManager(_ type: Int) {
            switch type {
            case 0: subscribe = .unfollow
            default: subscribe = .follow
            }
        }
        
        mutating func eventStreamsIsLiveManager(_ type: Int) {
            switch type {
            case 0: isLive = false
            default: isLive = true
            }
        }
        
    }
    
    var eventMembers = [eventsData?]()
    
    var selectedCell = IndexPath()
    
    var subtitle = String()


    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var collectionViewChampsh: UICollectionView!
    
    
    var width: CGFloat!
    var height: CGFloat!
    
    var selected = false
    var isHorizontal = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        createActivityView()
        parseEvents()
        setSubscribeImage()
        self.navigationController?.delegate = self
    }

    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupView()
    }
    
    
    func setCollectionViewLayoutDirection(_ direction: Direction) {
        
        switch direction {
        case .hor :
            width = (view.bounds.size.width - 90 * 2 - 2 * 8) / 4
            height = (view.bounds.size.width - 10 * 4) / 3.5
            isHorizontal = true
        case .ver :
            width = (view.bounds.size.width - 90 * 2 - 2 * 4) / 3
            height = (view.bounds.size.width - 20 * 4) / 2.6
            isHorizontal = false
        }
        kMainQueue.async {
            self.collectionViewChampsh.collectionViewLayout.invalidateLayout()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && selected == true {
            selected = false
        } else {
            selected = true
        }
        
        selectedCell = indexPath
        
        self.collectionViewChampsh.moveItem(at: indexPath, to: IndexPath(row: 0, section: 0))
        reverseArray(from: indexPath.row, to: 0)
        collectionViewChampsh.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        collectionViewChampsh.collectionViewLayout.invalidateLayout()
        collectionViewChampsh.reloadData()
      
        
        
    }
    
    
    func reverseArray(from item: Int, to: Int) {
        let firstReverse = eventMembers[item]
        eventMembers.remove(at: item)
        eventMembers.insert(firstReverse, at: to)
    }

    
    
    
    @IBAction func games(_ sender: UIButton) {
        let _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventMembers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewChampsh.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ListChmpshCVCellPad
        cell.prizeFund.text = "$ " + (eventMembers[indexPath.row]?.prizeFund)!
        cell.title.text = eventMembers[indexPath.row]?.events
        
        cell.imageField.image = UIImage(named: "loadingPad")
        
        //Download Image
        let urlString = eventMembers[indexPath.row]!.imageLink
        downloadImage.downloadImages(urlString, ifPhotoAvailable: { (image) in
            cell.imageField.image = image
        }) { (image) in
            cell.imageField.alpha = 0
            cell.imageField.image = image
            UIView.animate(withDuration: 0.3, animations: {
                cell.imageField.alpha = 1
            })
        }
        cell.dateStart.text = eventMembers[indexPath.row]!.startDate
        cell.dateEnd.text = eventMembers[indexPath.row]!.endDate
        cell.eventDescription.text = eventMembers[indexPath.row]!.description
        cell.eventDescription.contentOffset = CGPoint.zero
        
        switch eventMembers[indexPath.row]!.isLive {
        case true: cell.isLive.isHidden = false
        case false: cell.isLive.isHidden = true
        }
        
       
            cell.eventDescription.alpha = 1
            cell.startEndDateView.alpha = 1
            cell.tournamentBracket.alpha = 1
            cell.goToStreams.alpha = 1

            
            return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0 && selected == true {
            
            let widthSelected = collectionViewChampsh.frame.size.width - 90 * 2
            let heightSelected = isHorizontal ? collectionViewChampsh.frame.size.height - height - 10 : collectionViewChampsh.frame.size.height - height * 2 - 10
            
             return CGSize(width: widthSelected, height: heightSelected)
        } else {
        
        return CGSize(width: width, height: height)
        }
        
    }
    
    
    
    @IBOutlet weak var subscriber: UIButton!
    
    @IBAction func subscriber(_ sender: AnyObject) {
        notificationSubscribe()
    }

    
    func setupView() {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundTV")!)
        
        subscriber.layer.shadowColor = UIColor.black.cgColor
        subscriber.layer.shadowOpacity = 0.5
        subscriber.layer.shadowRadius = 3
        subscriber.layer.shadowOffset = CGSize(width: 0, height: 5)
        subscriber.layer.masksToBounds = false
        
        
 
        let oritntation = view.checkOrientation()
        setCollectionViewLayoutDirection(oritntation)
        
    }
    
    func createActivityView() {
        let viewWidth = view.frame.size.height / 12
        activityIndicator.circleTime = 2
        activityIndicator.frame.size = CGSize(width: viewWidth, height: viewWidth)
        activityIndicator.strokeColor = UIColor(red: 255/255, green: 167/255, blue: 52/255, alpha: 1)
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.animating = true
    }
    
    func setSubscribeImage() {
        let image = isFollow ? UIImage(named: "bellIsFollowed") : UIImage(named: "bellIsUnfollowed")
        if subscriber != nil {
            subscriber.setImage(image, for: .normal)
        }
    }
    

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController == self {
            setupView()
        }
    }
    
    func UIErrorDisplay(text toDisplay: String) {
        let viewWithErrorText = ErrorView(frame: collectionViewChampsh.bounds, textToShow: toDisplay)
        self.activityIndicator.animating = false
        collectionViewChampsh.addSubview(viewWithErrorText)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
 */

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStreams" {
            let destVC = segue.destination as! ListStreamsPad
         
            destVC.linkXMLToParse = (eventMembers[0]?.eventStreamsXML)!
            
            destVC.subtitle = eventMembers[selectedCell.row]!.subtitle
            
            destVC.index = selectedCell.row
            destVC.parentVC = self
            
            switch eventMembers[selectedCell.row]!.subscribe {
            case .follow: destVC.isFollow = true
            case .unfollow: destVC.isFollow = false
            }
            
        } else if segue.identifier == "tournamentsBracket" {
            let destVC = segue.destination as! TournamentBacketVCPad
            destVC.linkXMLToParse = (eventMembers[0]?.linkXMLTournament)!
            
        }
    }
 

}


extension ListChampshPad: XMLParserDelegate {
    
    func XMLParserError(_ error: String) {
        kMainQueue.async {
            self.UIErrorDisplay(text: "There are problems with the connection to the server")
        }
    }
    
    func parseEvents() {
        parser = XMLParser(url: linkXMLToParse)
        parser.delegate = self
        
        parser.parse {
            kMainQueue.async {
                self.activityIndicator.animating = false
            }
            
                guard self.parser.objects.count != 0 else {
                    self.UIErrorDisplay(text: "There are no events available yet")
                    return
                }
            
                for i in 0..<self.parser.objects.count {
                    self.eventMembers.append(eventsData(
                        events: self.parser.objects[i]["title"]!.clearSpace,
                        imageLink: self.parser.objects[i]["imgPad"]!.toUTF8,
                        prizeFund: self.parser.objects[i]["PrizeFund"]!,
                        eventStreamsXML: self.parser.objects[i]["linkXMLToStreams"]!.toUTF8,
                        subtitle: self.parser.objects[i]["subtitle"]!.toUTF8,
                        startDate: self.parser.objects[i]["StartDate"]!,
                        endDate: self.parser.objects[i]["EndDate"]!,
                        description: self.parser.objects[i]["Description"]!,
                        linkXMLTournament: self.parser.objects[i]["linkXMLTournamentBracket"]!.toUTF8,
                        subscribe: .unfollow,
                        isLive: false
                    ))
                    
                    self.eventMembers[i]?.subscribeManager(Int(self.parser.objects[i]["subscribe"]!.toUTF8)!)
                    
                    self.eventMembers[i]?.eventStreamsIsLiveManager(Int(self.parser.objects[i]["live"]!.toUTF8)!)
                }
            self.collectionViewChampsh.reloadData()
        }
        
    }
    

}


//Notification

extension ListChampshPad {
    func notificationSubscribe() {
        alertNotifyView = NotificationViewController(nibName: "NotificationView", bundle: nil)
        
        alertNotifyView?.isFollowed = isFollow

        alertNotifyView?.centerOfTheStart = subscriber.center
        
        alertNotifyView?.referanceVC = self
        alertNotifyView?.linkToSubscribe = linkXMLToParse
        alertNotifyView?.textToAlert = "Do you want to " + (isFollow ? "unfollow " : "follow ") + subtitle + "?"
        
        
        alertNotifyView!.view.frame = self.view.frame
        alertNotifyView!.showInView(self.view)
        
        
    }
}
