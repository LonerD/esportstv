//
//  TournamentBracketVC.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 15.07.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class TournamentBracketVC: UIViewController {
    
    var parser: XMLParser!
    
    //Outlets
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBOutlet weak var segmentControl: UIView!
    
    struct commands {
        var commandOne: String
        var commandOneImg: String
        var commandOneScrore: String
        var commandTwo: String
        var commandTwoImg: String
        var commandTwoScrore: String
        var date: String
        var time: String
        var bestOf: String
    }
    
    
    
    let downloadImage = ImageDownloader()
    
    var groups = [String : [commands]]()
    var playOff = [String : [commands]]()
    
    var playOffShow = Bool()
    var sectionTint: [UIColor]!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var linkXMLToParse: String!
    
    var navigationTitle = String()
    
    
    //LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.segmentControl.layer.opacity = 0
        
        
        title = navigationTitle + " BRACKET"
        self.parseTornaments(linkXMLToParse)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
        animate(playOffShow, false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.parser.abortParsing()
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    //Change Buttons
    
    @IBOutlet weak var changerConstr: NSLayoutConstraint!
    
    @IBOutlet weak var groupMatches: UIButton!
    @IBAction func groupMatches(_ sender: UIButton) {
        animate(false)
    }
    
    @IBOutlet weak var playoffMatches: UIButton!
    @IBAction func playoffMatches(_ sender: UIButton) {
        animate(true)
    }
    
    func animate(_ playOffShowing: Bool, _ reload: Bool = true) {
        UIView.animate(withDuration: 0.3, animations: {
            switch playOffShowing {
            case false:
                self.groupMatches.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.playoffMatches.transform = CGAffineTransform.identity
                self.playOffShow = false
            case true:
                self.groupMatches.transform = CGAffineTransform.identity
                self.playoffMatches.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.playOffShow = true
            }
            self.changerConstr.constant = UIScreen.main.bounds.size.width / 2 * CGFloat(playOffShowing.hashValue)
            
        
        }) 
        if reload {
            UIView.animate(withDuration: 0.3, animations: { 
                self.view.layoutIfNeeded()
            })
                sectionTint = []
                self.collectionView.setContentOffset(CGPoint.zero, animated: true)
            
                self.collectionView.reloadData()
            

        }
        
    }
    
}

extension TournamentBracketVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let sectionsNumber = playOffShow ? playOff.keys.count : groups.keys.count
        
        return sectionsNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let keys = playOffShow ? [String] (playOff.keys).sorted() : [String] (groups.keys).sorted()
        
        let countForEachKey = playOffShow ? playOff[keys[section]]?.count : groups[keys[section]]?.count
        
        
        let rowsNumb = countForEachKey ?? 0
        
        
        
        
        return rowsNumb
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView?.frame)!.width - 20, height: 143)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "section", for: indexPath) as! SectionReusableView
        
        let keys = playOffShow ? [String] (playOff.keys).sorted() : [String] (groups.keys).sorted()
        
        reusableView.title.text = keys[(indexPath as NSIndexPath).section]
        
        return reusableView
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TournamentCustomCell
        
        let keys = playOffShow ? [String] (playOff.keys).sorted() : [String] (groups.keys).sorted()
        
        let dataInSection = playOffShow ? playOff[keys[indexPath.section]] : groups[keys[(indexPath as NSIndexPath).section]]
        
        let cestDate = dataInSection![indexPath.row].date.clearSpace + " " + dataInSection![indexPath.row].time.clearSpace
        
        cell.timeTournament.text = cestDate.cestToCurrentTime.0 ?? dataInSection![indexPath.row].time.clearSpace + " CEST"
        cell.dateTournament.text = cestDate.cestToCurrentTime.1 ?? dataInSection![indexPath.row].date.clearSpace
        
        cell.bestOf.text = "Best of " + dataInSection![indexPath.row].bestOf
        
        let urlStringCommOne = dataInSection![indexPath.row].commandOneImg.clearSpace.toUTF8
        
        cell.firstCommandImg.image = UIImage(named: "loading")
        cell.secondCommandImg.image = UIImage(named: "loading")
        
        downloadImage.downloadImages(urlStringCommOne, ifPhotoAvailable: { (image) in
            cell.firstCommandImg.image = image
        }) { (image) in
            cell.firstCommandImg.alpha = 0
            cell.firstCommandImg.image = image
            UIView.animate(withDuration: 0.3, animations: {
                cell.firstCommandImg.alpha = 1
            })
        }
        
        
        
        cell.firstCommandScore.text = dataInSection![indexPath.row].commandOneScrore
        cell.firstCommandTitle.text = dataInSection![indexPath.row].commandOne
        
        let imgUrlCommTwo = dataInSection![indexPath.row].commandTwoImg.clearSpace.toUTF8
        
        downloadImage.downloadImages(imgUrlCommTwo, ifPhotoAvailable: { (image) in
            cell.secondCommandImg.image = image
        }) { (image) in
            cell.secondCommandImg.alpha = 0
            cell.secondCommandImg.image = image
            UIView.animate(withDuration: 0.3, animations: {
                cell.secondCommandImg.alpha = 1
            })
        }
        
        cell.secondCommandScore.text = dataInSection![indexPath.row].commandTwoScrore
        cell.secondCommandTitle.text = dataInSection![indexPath.row].commandTwo
        
        
        if sectionTint.indices.contains(indexPath.section) {
            cell.backgroundColor = sectionTint[indexPath.section]
            cell.timeInfoSubview.backgroundColor = sectionTint[indexPath.section]
        } else {
            while !sectionTint.indices.contains(indexPath.section) {
                let colorSection = UIColor().randomColor(sectionTint).withAlphaComponent(0.3)

                sectionTint.append(colorSection)
            } 
            
            
                cell.backgroundColor = sectionTint[indexPath.section]
                cell.timeInfoSubview.backgroundColor = sectionTint[indexPath.section]
            
            
        }
        
        
        
        
        return cell
    }
    
    
    
}

extension TournamentBracketVC: XMLParserDelegate {
    
    func XMLParserError(_ error: String) {
        let errorView = ErrorView(frame: self.view.frame, textToShow: "There are problems with the connection to the server")
        view.addSubview(errorView)
        
    }
    
    func parseTornaments(_ linkToParse: String) {
        parser = XMLParser(url: linkToParse)
        parser.delegate = self
        
        transactionAnimation(0.3, { () in
            self.progressBar.setProgress(0.3, animated: true)
        })
        
        self.parser.parse {
            
            self.transactionAnimation(0.3, { () in
                self.progressBar.setProgress(0.8, animated: true)
            })
            
            for i in 0..<self.parser.objects.count {
                
                if self.parser.objects[i]["Category"]?.clearSpace == "GROUPS" {
                    if self.groups[self.parser.objects[i]["section"]!] == nil {
                        
                        self.groups[self.parser.objects[i]["section"]!] = [commands(
                            commandOne: self.parser.objects[i]["commandOne"]!,
                            commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                            commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                            commandTwo: self.parser.objects[i]["commandTwo"]!,
                            commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                            commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                            date: self.parser.objects[i]["date"]!,
                            time: self.parser.objects[i]["time"]!,
                            bestOf: self.parser.objects[i]["bestOf"]!)]
                    } else {
                        var sectionCommands = self.groups[self.parser.objects[i]["section"]!]
                        sectionCommands?.append(commands(
                            commandOne: self.parser.objects[i]["commandOne"]!,
                            commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                            commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                            commandTwo: self.parser.objects[i]["commandTwo"]!,
                            commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                            commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                            date: self.parser.objects[i]["date"]!,
                            time: self.parser.objects[i]["time"]!,
                            bestOf: self.parser.objects[i]["bestOf"]!))
                        
                        self.groups[self.parser.objects[i]["section"]!] = sectionCommands
                        
                    }
                    
                } else if self.parser.objects[i]["Category"]?.clearSpace == "PLAYOFF" {
                    
                    if self.playOff[self.parser.objects[i]["section"]!] == nil {
                        
                        self.playOff[self.parser.objects[i]["section"]!] = [commands(
                            commandOne: self.parser.objects[i]["commandOne"]!,
                            commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                            commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                            commandTwo: self.parser.objects[i]["commandTwo"]!,
                            commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                            commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                            date: self.parser.objects[i]["date"]!,
                            time: self.parser.objects[i]["time"]!,
                            bestOf: self.parser.objects[i]["bestOf"]!)]
                        
                    } else {
                        var sectionCommands = self.playOff[self.parser.objects[i]["section"]!]
                        sectionCommands?.append(commands(
                            commandOne: self.parser.objects[i]["commandOne"]!,
                            commandOneImg: self.parser.objects[i]["commandOneImg"]!,
                            commandOneScrore: self.parser.objects[i]["commandOneScore"]!,
                            commandTwo: self.parser.objects[i]["commandTwo"]!,
                            commandTwoImg: self.parser.objects[i]["commandTwoImg"]!,
                            commandTwoScrore: self.parser.objects[i]["commandTwoScore"]!,
                            date: self.parser.objects[i]["date"]!,
                            time: self.parser.objects[i]["time"]!,
                            bestOf: self.parser.objects[i]["bestOf"]!))
                        
                        self.playOff[self.parser.objects[i]["section"]!] = sectionCommands
                    }
                }
            }
            
            self.animateEmptyList()
            
        }
    }
    
    func animateEmptyList() {
        if self.groups.count == 0 && self.playOff.count == 0  {
            let errorView = ErrorView(frame: self.view.frame, textToShow: "Tournament bracket isn't available yet")
            self.view.addSubview(errorView)
            
            
            let delayTime = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.transactionAnimation(1, { () in
                    self.progressBar.setProgress(0, animated: true)
                })
            }

            
            
        } else {

            transactionAnimation(0.3, { () in
                self.progressBar.setProgress(1, animated: true)
                self.segmentControl.layer.opacity = 1
            })
            
            let delayTime = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.transactionAnimation(0.3, { () in
                    self.progressBar.alpha = 0
                })
            }
            checkingContentAvailability()
        }
        
    }
    
    
    func checkingContentAvailability() {
        
        switch self.groups.count {
        case 0: playOffShow = true
            groupMatches.isEnabled = false
            groupMatches.alpha = 0.5
        default:
            playOffShow = false
            if playOff.count == 0 {
                playoffMatches.isEnabled = false
                playoffMatches.alpha = 0.5
            }
        }
        
        animate(self.playOffShow)
    }
    
    
    func transactionAnimation(_ duration: CFTimeInterval, _ animations: @escaping (Void) -> ()) {
        CATransaction.begin()
        
        CATransaction.setCompletionBlock({
          UIView.animate(withDuration: duration, animations: {
            animations()
          }) 
            })
        CATransaction.commit()
    }
    
}









