//
//  XMLParser.swift
//  XMLParsing
//
//  Created by Дмитрий Бондаренко on 12.03.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

protocol XMLParserDelegate {
    func XMLParserError(_ error: String)
}

class XMLParser: NSObject, Foundation.XMLParserDelegate {
    
    let url: URL
    var delegate: XMLParserDelegate?
    
    var objects = [[String: String]]()
    
    var object = [String: String]()
    
    var inItem = false
    var current = String()
    var parser = Foundation.XMLParser() { didSet { self.parser.delegate = self } }
    
    var handler: (() -> Void)?
    
    init(url: String) {
        self.url = URL(string: url + "?token=" + token!)!
        super.init()
        self.deleteObjects()
    }
    
    func deleteObjects() {
        objects.removeAll(keepingCapacity: false)
        object.removeAll(keepingCapacity: false)
        inItem = false
        handler = nil
    }
    
    func parse(_ handler: @escaping () -> Void) {
        self.handler = handler
        kBgQ.async  {
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            if let xmlCode = try? Data(contentsOf: self.url) {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                self.parser = Foundation.XMLParser(data: xmlCode)
                self.parser.parse()
            } else {
                self.delegate?.XMLParserError("You have a problems with connection!")
            }
        }
    }
    
    
    func abortParsing() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if parser.parse() {
            parser.abortParsing()
        }
    }
    
    
     func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        delegate?.XMLParserError(parseError.localizedDescription)
    }
    
     func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == "item" {
            object.removeAll(keepingCapacity: false)
            inItem = true
        }
        current = elementName
        
    }
    
     func parser(_ parser: XMLParser, foundCharacters string: String) {
        if !inItem {
            return
        }
        if let temp = object[current] {
            var tempString = temp
            tempString += string
            object[current] = tempString
        } else {
            object[current] = string
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            inItem = false
            objects.append(object)
        }
    }
    
      func parserDidEndDocument(_ parser: XMLParser) {
        DispatchQueue.main.async {
            if self.handler != nil {
                self.handler!()
                
            }
        }
    }
    
    
}
