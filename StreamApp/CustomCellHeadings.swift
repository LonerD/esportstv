//
//  CustomCellHeadings.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 21.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class CustomCellHeadings: UITableViewCell {
    @IBOutlet weak var imageNamesOfTheHeadings: UIImageView!
}
