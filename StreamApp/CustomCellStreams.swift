//
//  CustomCellStreams.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 25.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class CustomCellStreams: UITableViewCell {

    @IBOutlet weak var imageStreams: UIImageView!
    @IBOutlet weak var titleStreams: UILabel!
    @IBOutlet weak var languageStream: UILabel!
    @IBOutlet weak var separator: UIImageView!
}
