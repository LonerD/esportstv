//
//  ContentViewController.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 21.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import SafariServices

class ContentViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var pageIndex: Int!
    var imageFile: String!
    var linksStream = [String]()
    var startViewController = UIViewController()
    var advLink = String()
    
    var downloader = ImageDownloader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImage()
    }
    
    @IBAction func buttonGoStream(_ sender: AnyObject) {
        if advLink != "nil" {
            if let url = URL(string: advLink) {
                if #available(iOS 9.0, *) {
                    let safaryVC = SFSafariViewController(url: url)
                    safaryVC.delegate = self
                    UIApplication.shared.keyWindow?.rootViewController?.present(safaryVC, animated: true, completion: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            
            }
        } else {
            performSegue(withIdentifier: "showStreamFromSlider", sender: nil)
        }
    }
    
    func loadImage() {
        self.imageView.alpha = 0
        downloader.downloadImages(imageFile, ifPhotoAvailable: { (image) in
            self.imageView.image = image
            UIView.animate(withDuration: 0.3, animations: {
                self.imageView.alpha = 1
            }) 
        }) { (image) in
            self.imageView.image = image
            UIView.animate(withDuration: 0.3, animations: {
                self.imageView.alpha = 1
            }) 
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStreamFromSlider" {
            let destVC = segue.destination as? Player
            destVC?.linksArray = linksStream
        }
    }
    
}


extension ContentViewController: SFSafariViewControllerDelegate {
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
