//
//  ListChmpshCVCellPad.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 13.09.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class ListChmpshCVCellPad: UICollectionViewCell {

    @IBOutlet weak var imageField: UIImageView!
    
    @IBOutlet weak var title: UILabel!

    @IBOutlet weak var prizeFund: UILabel!
    
    
    @IBOutlet weak var tournamentBracket: UIView!
    @IBOutlet weak var goToStreams: UIButton!

    @IBOutlet weak var isLive: UIImageView!
    @IBOutlet weak var dateEnd: UILabel!
    @IBOutlet weak var dateStart: UILabel!
    @IBOutlet weak var startEndDateView: UIView!
    
    @IBOutlet weak var eventDescription: UITextView!

    @IBOutlet weak var imageBackgroundView: UIView!

}
