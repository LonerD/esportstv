//
//  startScreen.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 28.04.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class StartScreen: UIViewController {
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBOutlet weak var reloadParseO: UIButton!
    @IBOutlet weak var textReloadO: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notifCentr.addObserver(self, selector: #selector(StartScreen.setProgress(_:)), name: NSNotification.Name(rawValue: "passDataInView"), object: nil)
        notifCentr.addObserver(self, selector: #selector(StartScreen.parseError), name: NSNotification.Name(rawValue: "parseError"), object: nil)
        
        settingsForReloadButton()
    }
    
    func parseError() {
        progressBarSetProgress() { (handler) in
            UIView.animate(withDuration: 0.3, animations: {
                self.reloadParseO.alpha = 1
                self.textReloadO.alpha = 1
            }) 
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        progressBarSetProgress(0.2)
    }
    
    func setProgress(_ notification: Notification) {
        let number: Float = (notification as NSNotification).userInfo!["progress"] as! Float
        progressBarSetProgress(number)
    }
    
    @IBAction func reloadParse(_ sender: AnyObject) {
        UIView.animate(withDuration: 0.3, animations: {
            self.reloadParseO.alpha = 0
            self.textReloadO.alpha = 0
        }) 
        notifCentr.post(name: Notification.Name(rawValue: "reloadCategories"), object: nil)
    }
    
    func settingsForReloadButton() {
        reloadParseO.setBackgroundImage(UIImage(named: "white-reload"), for: UIControlState())
        reloadParseO.setBackgroundImage(UIImage(named: "yellow-reload"), for: .highlighted)
        reloadParseO.alpha = 0
        textReloadO.alpha = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notifCentr.removeObserver(self, name: Notification.Name(rawValue: "passDataInView"), object: nil)
        notifCentr.removeObserver(self, name: Notification.Name(rawValue: "parseError"), object: nil)
    }
    
    func progressBarSetProgress(_ number: Float = 0, handler: ((Void) -> ())? = nil) {
        kMainQueue.async {
            self.progressBar.setProgress(number, animated: true)
            if handler != nil {
                handler!()
            }
        }
    }
}
