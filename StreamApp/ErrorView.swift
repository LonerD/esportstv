//
//  ErrorView.swift
//  StreamApp
//
//  Created by Дмитрий Бондаренко on 16.08.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class ErrorView: UIView {
    
    var textToShow: String!
    
    init(frame: CGRect, textToShow: String) {
        self.textToShow = textToShow
        super.init(frame: frame)
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupView() {
        let text = UILabel()
        
        text.frame.size = CGSize(width: self.frame.size.width - 20, height: self.frame.size.height)
        text.center = self.center
        text.backgroundColor = UIColor.clear
        text.text = textToShow
        text.font = UIFont(name: "HeliosCond", size: 21)
        text.numberOfLines = 0
        text.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 170/255)
        text.textAlignment = .center
        
        self.addSubview(text)
    }
    
    
}
